/** @type {import('next').NextConfig} */
// const { i18n } = require("./ i18n.config");
const nextConfig = {
  webpack: (config, { isServer }) => {
    // Bạn chỉ muốn cấu hình loader trên phía máy chủ khi sử dụng Next.js
    if (isServer) {
      const rule = {
        test: /\.(woff2|woff|ttf)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "static/fonts/", // Đường dẫn đích của font sau khi build
            publicPath: "/_next/static/fonts/", // Đường dẫn truy cập từ client
          },
        },
      };
      config.module.rules.push(rule);
    }

    return config;
  },
};

module.exports = nextConfig;
