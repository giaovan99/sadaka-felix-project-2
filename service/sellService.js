import { https } from "@/config/baseUrl";
export const sellService = {
  getProductSell: async (data) =>
    await https.get(
      `/estates_sell?limit=${data.limit}&page=${data.page}&keywords=${data.keywords}&min_unit_price=${data.min_unit_price}&max_unit_price=${data.max_unit_price}&district_id=${data.district_id}&min_area=${data.min_area}&max_area=${data.max_area}`
    ),
  getProductSellDetail: async (id) => await https.get(`/estates_sell/${id}`),
};
