import { https } from "@/config/baseUrl";
export const langugeService = {
  getLanguage: async () => await https.get(`/language`),
};
