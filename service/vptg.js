import { https } from "@/config/baseUrl";

export const vptgService = {
  getCorp: async () => await https.get("/vptg"),
};
