import { https } from "@/config/baseUrl";

export const productService = {
  getProduct: async (data) =>
    await https.get(
      `/estates?limit=${data.limit}&page=${data.page}&keywords=${data.keywords}&type=${data.type}&min_unit_price=${data.min_unit_price}&max_unit_price=${data.max_unit_price}&district_id=${data.district_id}&min_area=${data.min_area}&max_area=${data.max_area}&ward=${data?.ward}`
    ),
  getProductDetail: async (id) => await https.get(`/estates/${id}`),
  getProductType: async () => await https.get(`/estates_group`),
  getDistrict: async () => await https.get(`/geo_district`),
  getSlugs: async () => await https.get(`/all-slug`),
};
