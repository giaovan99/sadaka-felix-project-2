import { NextResponse } from "next/server";

let locales = ["en", "vi"];
let lastLocale;
export function middleware(request) {
  const pathname = request.nextUrl.pathname;
  // Nếu là trang sitemap
  if (pathname == "/sitemap.xml") {
    return;
  }
  const pathnameIsMissingLocale = locales.every(
    (locale) => !pathname.startsWith(`/${locale}/`) && pathname !== `/${locale}`
  );
  if (pathnameIsMissingLocale) {
    lastLocale = "vi";
    return NextResponse.redirect(new URL(`/vi${pathname}`, request.url));
  }
}
export const config = {
  matcher: [
    // Skip all internal paths (_next)
    "/((?!_next).*)",
    // Optional: only run on root (/) URL
    // '/'
  ],
};
