import { productService } from "@/service/productService";
const BASE_URL = "https://felixoffice.vn";

export default async function sitemap() {
  let fixPage = [
    {
      url: BASE_URL + "/",
      lastModified: new Date(),
      changeFrequency: "weekly",
      priority: 1,
    },
    {
      url: BASE_URL + "/ban",
      lastModified: new Date(),
      changeFrequency: "daily",
      priority: 0.7,
    },
    {
      url: BASE_URL + "/lien-he",
      lastModified: new Date(),
      changeFrequency: "never",
      priority: 0.7,
    },
    {
      url: BASE_URL + "/mat-bang-kinh-doanh",
      lastModified: new Date(),
      changeFrequency: "never",
      priority: 0.7,
    },
    {
      url: BASE_URL + "/san-van-phong",
      lastModified: new Date(),
      changeFrequency: "never",
      priority: 0.7,
    },
    {
      url: BASE_URL + "/toa-nha",
      lastModified: new Date(),
      changeFrequency: "never",
      priority: 0.7,
    },
    {
      url: BASE_URL + "/van-phong-tron-goi",
      lastModified: new Date(),
      changeFrequency: "never",
      priority: 0.7,
    },
  ];
  let res = await productService.getSlugs();
  let arr = [];
  if (res.data.success) {
    res.data.data.forEach((item) => {
      arr.push({
        url: item
          .replace(/&/g, "&amp;")
          .replace(/'/g, "&apos;")
          .replace(/"/g, "&quot;")
          .replace(/>/g, "&gt;")
          .replace(/</g, "&lt;"),

        lastModified: new Date(),
        changeFrequency: "never",
        priority: 0.8,
      });
    });
  }
  return [...fixPage, ...arr];
}
