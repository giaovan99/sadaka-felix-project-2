import { configureStore } from "@reduxjs/toolkit";
import languageSlice from "./slice/languageSlice";

export const store = configureStore({
  reducer: {
    languageSlice,
  },
});
