// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  vi: {},
  en: {},
};

export const languageSlice = createSlice({
  name: "languageChange",
  initialState,
  reducers: {
    setLanguageVi: (state, actions) => {
      // console.log(actions.payload);
      state.vi = actions.payload;
    },
    setLanguageEn: (state, actions) => {
      state.en = actions.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setLanguageVi, setLanguageEn } = languageSlice.actions;

export default languageSlice.reducer;
