"use client";
import React, { useEffect, useState } from "react";
import "../../../../css/search.css";
import TitleSearchPage from "../../components/SearchPage/TitleSearchPage/TitleSearchPage";
import RenderListSearchPage from "../../components/SearchPage/RenderListSearchPage/RenderListSearchPage";
import LienHeSearchPage from "../../components/SearchPage/LienHeSearchPage/LienHeSearchPage";
import { sellService } from "@/service/sellService";
import { getDictionary } from "../../distionaries";

export const dynamic = "force-dynamic";

export default function Page({ searchParams, params: { lang } }) {
  const [arrListPage, setArrListPage] = useState([]);
  const [total, setTotal] = useState();
  const [pageNum, setPageNum] = useState(1);
  const [dict, setDict] = useState({});

  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);
  let {
    keywords,
    district_id,
    type,
    min_unit_price,
    max_unit_price,
    limit,
    min_area,
    max_area,
    page,
  } = searchParams;
  useEffect(() => {
    if (page) {
      setPageNum(page);
    } else {
      setPageNum(1);
    }
  }, [page]);
  // lấy danh sách sản phẩm
  let getProductSearchList = async () => {
    // check params
    if (!min_unit_price) {
      min_unit_price = "";
    }
    if (!max_unit_price) {
      max_unit_price = "";
    }
    if (!limit) {
      limit = "";
    }
    if (!type) {
      type = "";
    }
    if (!district_id) {
      district_id = "";
    }
    if (!keywords) {
      keywords = "";
    }
    if (!min_area) {
      min_area = "";
    }
    if (!max_area) {
      max_area = "";
    }
    if (!page) {
      page = 1;
    }

    try {
      let res = await sellService.getProductSell({
        limit: 9,
        page: page,
        keywords: keywords,
        type: type,
        district_id: district_id,
        min_unit_price: min_unit_price,
        max_unit_price: max_unit_price,
        min_area: min_area,
        max_area: max_area,
      });

      if (res.data.code == 200) {
        setArrListPage(res.data.data);
        setTotal(res.data.total);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getProductSearchList();
  }, [page]);

  return (
    <section id="search">
      {/* Title */}
      <TitleSearchPage type={type} dict={dict} />
      {/* Render List */}
      <RenderListSearchPage
        arrListProduct={arrListPage}
        total={total}
        page={page}
        estates={"estates_sell"}
        dict={dict}
      />
      {/* Liên hệ */}
      <LienHeSearchPage dict={dict} />
    </section>
  );
}
