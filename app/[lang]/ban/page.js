"use client";
import React, { useEffect, useState } from "react";
import ProductListHeader from "../components/ProductList/ProductListHeader/ProductListHeader";
import ProductListSearch from "../components/ProductList/ProductListSearch/ProductListSearch";
import ProductListSubTiltle from "../components/ProductList/ProductListSubTiltle/ProductListSubTiltle";
import ProductListChooseDistrict from "../components/ProductList/ProductListChooseDistrict/ProductListChooseDistrict";
import ProductListRender from "../components/ProductList/ProductListRender/ProductListRender";
import ProductListLienHe from "../components/ProductList/ProductListLienHe/ProductListLienHe";
import ProductListEndPage from "../components/ProductList/ProductListEndPage/ProductListEndPage";
import { productService } from "@/service/productService";
import { sellService } from "@/service/sellService";
import { getDictionary } from "../distionaries";

export default function Page({ params: { lang } }) {
  // console.log(lang);
  const [dict, setDict] = useState({});
  const { sell } = dict;

  // danh sách các quận có tài sảm khi get tất cả sản phẩm
  const [districts, setDistricts] = useState([]);
  // danh sách tất cả các quận tại TPHCM
  const [districtsList, setDistrictsList] = useState([]);
  // danh sách id các quận khi khách hàng chọn quận nhất định
  const [listDistricActive, setListDistricActive] = useState([]);
  // số lượng kết quả
  const [result, setResult] = useState(0);
  // loading page
  const [loading, setLoading] = useState(true);

  let checkActiveDistrict = (id) => {
    // check id quận đã có trong mảng chưa
    let index = listDistricActive.findIndex((item) => item == id);
    let cloneArr = [...listDistricActive];
    // nếu chưa, push id và mảng
    if (index == -1) {
      cloneArr.push(id);
    }
    // nếu rồi, xóa khỏi mảng
    else {
      cloneArr.splice(index, 1);
    }
    setListDistricActive(cloneArr);
  };

  // lấy danh sách sản phẩm
  let getProdustList = async () => {
    const res = await sellService.getProductSell({
      limit: "",
      page: "",
      type: "",
      type: "",
      keywords: "",
      district_id: listDistricActive.join(","),
      min_price: "",
      max_price: "",
    });
    setDistricts(res.data.relationship.districts);
    setResult(res.data.total);
    setLoading(false);
  };

  // lay danh sach tat ca cac quan o HCM
  let getDistrictList = async () => {
    const res = await productService.getDistrict();
    if (res.data.code == 200) {
      setDistrictsList(res.data.data);
    }
  };
  useEffect(() => {
    getProdustList();
    getDistrictList();
  }, []);

  useEffect(() => {
    getProdustList();
  }, [listDistricActive]);

  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);

  return (
    <section id="mat-bang-kinh-doanh">
      {/* Header */}
      <ProductListHeader text={sell?.buy_and_sell_in_ho_chi_minh_city} />
      {/* Search */}
      <ProductListSearch
        type=""
        districtsList={districtsList}
        estates={"estates_sell"}
        dict={dict}
      />
      {/* Sub Title */}
      <ProductListSubTiltle
        title={
          <>
            {sell?.receive_consignment_for_purchase_and_sale} <br />
            {sell?.whole_housebuildingvilla_in_city_hcm}
          </>
        }
        text={sell?.felix_office_accepts_consignments_to_buy_and_sell}
      />
      {/* Choose District */}
      <ProductListChooseDistrict
        checkActiveDistrict={checkActiveDistrict}
        listDistricActive={listDistricActive}
        arr={districtsList}
        setListDistricActive={setListDistricActive}
        result={result}
        setResult={setResult}
        loading={loading}
        setLoading={setLoading}
        dict={dict}
      />
      {/* Render */}
      <ProductListRender
        districts={districts}
        type=""
        estates={"estates_sell"}
        dict={dict}
      />
      {/* Contact  */}
      <ProductListLienHe
        dict={dict}
        question={dict?.general?.havent_found_the_right_space_yet}
      />
      {/* End Page */}
      <ProductListEndPage
        title={sell?.other_services}
        disc={sell?.in_addition_felix_office_also_provides_rental}
        text1={dict?.general?.office_floors}
        text2={dict?.general?.office_building}
        text3={dict?.general?.business_premises}
        type="sell"
        dict={dict}
        questionText={dict?.general?.you_have_not_found_the_right_space}
      />
    </section>
  );
}
