"use client";
import React, { useEffect, useState } from "react";
import "../../../css/lien-he.css";
import BannerLienHePage from "../components/LienHePage/BannerLienHePage/BannerLienHePage";
import SoLieuLienHePage from "../components/LienHePage/SoLieuLienHePage/SoLieuLienHePage";
import GiaTriLienHePage from "../components/LienHePage/GiaTriLienHePage/GiaTriLienHePage";
import TuVanLienHePage from "../components/LienHePage/TuVanLienHePage/TuVanLienHePage";
import DoiTac from "../components/DoiTac/DoiTac";
import { getDictionary } from "../distionaries";

export default function Page({ params: { lang } }) {
  const [dict, setDict] = useState({});
  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);

  // console.log(dict);
  return (
    <section id="lien-he">
      {/* Banner */}
      <BannerLienHePage dict={dict} />
      {/* Số liệu */}
      <SoLieuLienHePage dict={dict} />
      {/* Giá trị */}
      <GiaTriLienHePage dict={dict} />
      {/* Tư vấn */}
      <TuVanLienHePage dict={dict} />
      {/* Đối tác */}
      <DoiTac dict={dict} />
    </section>
  );
}
