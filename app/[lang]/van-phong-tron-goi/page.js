"use client";
import React, { useEffect, useState } from "react";
import "../../../css/van-phong.css";
import BannerVanPhongTronGoi from "../components/VanPhongTronGoi/BannerVanPhongTronGoi/BannerVanPhongTronGoi";
import DinhNghiaVanPhongTronGoi from "../components/VanPhongTronGoi/DinhNghiaVanPhongTronGoi/DinhNghiaVanPhongTronGoi";
import DacDiemVanPhongTronGoi from "../components/VanPhongTronGoi/DacDiemVanPhongTronGoi/DacDiemVanPhongTronGoi";
import LoiIchVanPhongTronGoi from "../components/VanPhongTronGoi/LoiIchVanPhongTronGoi/LoiIchVanPhongTronGoi";
import DoiTacVanPhongTronGoi from "../components/VanPhongTronGoi/DoiTacVanPhongTronGoi/DoiTacVanPhongTronGoi";
import TuVanVanPhongTronGoi from "../components/VanPhongTronGoi/TuVanVanPhongTronGoi/TuVanVanPhongTronGoi";
import { getDictionary } from "../distionaries";

export default function Page({ params: { lang } }) {
  const [dict, setDict] = useState({});
  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);

  // console.log(dict);
  return (
    <section id="van-phong-tron-goi">
      {/* Banner */}
      <BannerVanPhongTronGoi dict={dict} />
      {/* Định nghĩa */}
      <DinhNghiaVanPhongTronGoi dict={dict} />
      {/* Đặc điểm */}
      <DacDiemVanPhongTronGoi dict={dict} />
      {/* Lợi ích */}
      <LoiIchVanPhongTronGoi dict={dict} />
      {/* Đối tác */}
      <DoiTacVanPhongTronGoi dict={dict} />
      {/* Tư vấn */}
      <TuVanVanPhongTronGoi dict={dict} />
    </section>
  );
}
