export const metadata = {
  title: "Felix Office - Tòa nhà nguyên căn",
  description:
    "Felix Office cung cấp dịch vụ cho thuê văn phòng, tòa nhà và mặt bằng kinh doanh chuyên nghiệp hàng đầu tại TP. HCM. Giúp bạn tiết kiệm thời gian, tối ưu chi phí với điều kiện thuê tốt nhất!",
};

export default function ToaNhaLayout({
  children, // will be a page or nested layout
}) {
  return <section>{children}</section>;
}
