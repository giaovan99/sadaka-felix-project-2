"use client";
import Image from "next/image";
import DoiTac from "./components/DoiTac/DoiTac";
import "../../css/HomePage.css";
import Group from "../../public/img/img-home/Group.png";
import timMatBangImg from "../../public/img/img-home/Rectangle 93.jpg";
import Quotes from "./components/HomePage/QuoteCarousel/QuoteCarousel";
import Service from "./components/HomePage/ServiceHomePage/ServiceHomePage";
import BannerHomePage from "./components/HomePage/BannerHomePage/BannerHomePage";
import SelectSectionHomePage from "./components/HomePage/SelectSectionHomePage/SelectSectionHomePage";
import FormLienHe from "./components/FormLienHe/FormLienHe";
import { getDictionary } from "./distionaries";
import { useEffect, useState } from "react";

export default function HomePage({ params: { lang } }) {
  // const dict = await getDictionary(lang);
  // const { general, home } = dict;

  const renderListText = (arr) => {
    if (arr.length > 0) {
      return arr.map((item, index) => {
        return (
          <div className="list-ol" key={index}>
            <p className="num">{index + 1}</p>
            {/* <div className="text" dangerouslySetInnerHTML={{ __html: item }} /> */}
            <p className="text">{item}</p>
          </div>
        );
      });
    }
  };

  const [dict, setDict] = useState({});
  const { general, home } = dict;
  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    // console.log(res);
    if (res) {
      setDict(res);
    }
  };
  useEffect(() => {
    getDict(lang);
  }, [lang]);
  return (
    <section id="homePage">
      {dict && (
        <>
          {/* SelectHomePage */}
          <SelectSectionHomePage dict={dict} />
          {/* Banner */}
          <BannerHomePage dict={dict} />
          {/* Service */}
          <Service dict={dict} />
          {/* Quotes Carousel */}
          <Quotes dict={dict} />
          {/* Tìm mặt bằng */}
          <div className="timMatBang mb-16">
            <div className="container-1 flex flex-col-reverse md:grid md:grid-cols-2 text-and-img">
              <div className="left-side space-y-5 bg-[#EEF0EF] pl-4 pr-1 md:pl-20 md:pr-5 flex flex-col justify-center py-8 md:py-10">
                <div className="title title--section--2 ">
                  <h3>
                    {general?.help_you_quickly_find_the_right_office_and_space}
                  </h3>
                </div>
                <div className="list space-y-4 roboto-font">
                  {renderListText([
                    general?.get_quotation_for_all_suitable_space_on_request,
                    general?.get_detailed_advice_on_options_pros_and_cons,
                    general?.get_support_to_go_see_the_reality_completely_free,
                    general?.negotiate_contract_terms_and_best_rental_rates,
                  ])}
                </div>
                <div className="flex justify-center md:justify-start">
                  <FormLienHe
                    text={general?.button_open_contact_form}
                    dict={dict}
                  />
                </div>
              </div>
              <div className="right-side">
                <Image src={timMatBangImg} alt="..."></Image>
              </div>
            </div>
          </div>
          {/* Đối tác */}
          <DoiTac dict={dict} />
          {/* Chất lượng hàng đầu */}
          <div className="chatLuong flex flex-col justify-center items-center text-white space-y-6">
            <h3 className="title--section--3">
              {home?.top_quality}
              <br className="md:hidden" /> - {home?.absolute_satisfaction}
            </h3>
            <Image
              src={Group}
              width={235}
              height={63}
              alt="..."
              className="hidden md:block"
            />
          </div>
        </>
      )}
    </section>
  );
}
