"use client";
import React, { useEffect, useState } from "react";
import "../../../../css/chi-tiet.css";
import AvailabilityChiTietPage from "../../components/ChiTietPage/AvailabilityChiTietPage/AvailabilityChiTietPage";
import ImageChiTietPage from "../../components/ChiTietPage/ImageChiTietPage/ImageChiTietPage";
import MapChiTietPage from "../../components/ChiTietPage/MapChiTietPage/MapChiTietPage";
import GanDayChiTietPage from "../../components/ChiTietPage/GanDayChiTietPage/GanDayChiTietPage";
import { sellService } from "@/service/sellService";
import InfoChiTietPageMuaBan from "@/app/[lang]/components/ChiTietPage/InfoChiTietPage/InfoChiTietPageMuaBan";
import { getDictionary } from "../../distionaries";

export default function Page({ params }) {
  const [arr, setArr] = useState([]);
  const [item, setItem] = useState({});
  const [dict, setDict] = useState({});

  const { lang } = params;

  let getProductDetail = async () => {
    let res = await sellService.getProductSellDetail(params.slug);
    if (res.data.code == 200) {
      setItem(res.data.data);
    }
  };

  useEffect(() => {
    getProductDetail();
  }, []);

  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);

  let getGanNoiTimKiem = async () => {
    let res = await sellService.getProductSell({
      limit: 10,
      page: 1,
      type: "",
      keywords: "",
      district_id: item.district_id,
      min_price: "",
      max_price: "",
      ward: item?.ward,
    });
    if (res.data.code == 200) {
      let cloneArr = [...res.data.data];
      let index = cloneArr.findIndex((product) => item.id == product.id);
      if (index == -1) {
        setArr(cloneArr);
      } else {
        cloneArr.splice(index, 1);
        setArr(cloneArr);
      }
    }
  };

  useEffect(() => {
    getGanNoiTimKiem();
  }, [item]);

  return (
    <section id="chi-tiet" className=" bg-white">
      {/* Availability */}
      <AvailabilityChiTietPage item={item} />
      {/* Image Gallery */}
      {item?.gallery && <ImageChiTietPage gallery={item.gallery} />}
      {/* Info */}
      <InfoChiTietPageMuaBan item={item} dict={dict} />
      {/* Bản đồ */}
      {item?.link_google_map && <MapChiTietPage item={item} dict={dict} />}
      {/* Gần đây */}
      <GanDayChiTietPage
        item={item}
        arr={arr}
        estates="estates_sell"
        dict={dict}
      />
    </section>
  );
}
