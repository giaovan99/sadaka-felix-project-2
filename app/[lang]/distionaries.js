import { langugeService } from "@/service/languageService";

const dictionaries = {
  vi: () =>
    langugeService.getLanguage().then((res) => {
      return res.data.data.vi;
    }),
  en: () => langugeService.getLanguage().then((res) => res.data.data.en),
};

export const getDictionary = async (locale) => {
  if (locale === "en") {
    return dictionaries.en();
  }
  return dictionaries.vi();
};
