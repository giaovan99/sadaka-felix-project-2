"use client";
import React, { useEffect, useState } from "react";
import "../../../css/san-van-phong.css";
import ProductListHeader from "../components/ProductList/ProductListHeader/ProductListHeader";
import ProductListSearch from "../components/ProductList/ProductListSearch/ProductListSearch";
import ProductListRender from "../components/ProductList/ProductListRender/ProductListRender";
import ProductListChooseDistrict from "../components/ProductList/ProductListChooseDistrict/ProductListChooseDistrict";
import ProductListLienHe from "../components/ProductList/ProductListLienHe/ProductListLienHe";
import ProductListEndPage from "../components/ProductList/ProductListEndPage/ProductListEndPage";
import { productService } from "@/service/productService";
import { getDictionary } from "../distionaries";

export default function Page({ params: { lang } }) {
  // console.log(lang);
  const [dict, setDict] = useState({});
  const { office_floor } = dict;

  // danh sách các quận có tài sản khi get tất cả sản phẩm
  const [districts, setDistricts] = useState([]);
  // danh sách tất cả các quận tại TPHCM
  const [districtsList, setDistrictsList] = useState([]);
  // danh sách id các quận khi khách hàng chọn quận nhất định
  const [listDistricActive, setListDistricActive] = useState([]);
  // số lượng kết quả
  const [result, setResult] = useState(0);
  // loading page
  const [loading, setLoading] = useState(true);

  let checkActiveDistrict = (id) => {
    // check id quận đã có trong mảng chưa
    let index = listDistricActive.findIndex((item) => item == id);
    let cloneArr = [...listDistricActive];

    // nếu chưa, push id và mảng
    if (index == -1) {
      cloneArr.push(id);
    }

    // nếu rồi, xóa khỏi mảng
    else {
      cloneArr.splice(index, 1);
    }
    setListDistricActive(cloneArr);
  };

  // lấy danh sách sản phẩm
  let getProdustList = async () => {
    setLoading(true);
    const res = await productService.getProduct({
      limit: "",
      page: "",
      type: "office_floor",
      keywords: "",
      district_id: listDistricActive.join(","),
      min_price: "",
      max_price: "",
    });
    setDistricts(res.data.relationship.districts);
    setResult(res.data.total);
    setLoading(false);
  };

  // lay danh sach tat ca cac quan o HCM
  let getDistrictList = async () => {
    const res = await productService.getDistrict();
    if (res.data.code == 200) {
      setDistrictsList(res.data.data);
    }
  };

  useEffect(() => {
    getProdustList();
    getDistrictList();
  }, []);

  useEffect(() => {
    getProdustList();
  }, [listDistricActive]);

  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);

  return (
    <section id="san-van-phong">
      {/* Header */}
      <ProductListHeader text={office_floor?.title} />
      {/* Search */}
      <ProductListSearch
        type="office_floor"
        districtsList={districtsList}
        dict={dict}
      />
      {/* Choose District */}
      <ProductListChooseDistrict
        checkActiveDistrict={checkActiveDistrict}
        listDistricActive={listDistricActive}
        arr={districtsList}
        setListDistricActive={setListDistricActive}
        result={result}
        setResult={setResult}
        loading={loading}
        setLoading={setLoading}
        dict={dict}
      />
      {/* Render */}
      <ProductListRender
        districts={districts}
        type="office_floor"
        dict={dict}
      />
      {/* Contact  */}
      <ProductListLienHe
        dict={dict}
        question={dict?.office_floor?.you_have_not_found}
      />
      {/* End Page */}
      <ProductListEndPage
        title={dict?.office_floor?.all_inclusive_office_in_ho_chi_minh_city}
        text1={dict?.office_floor?.room_4_8_people}
        text2={dict?.office_floor?.room_8_12_people}
        text3={dict?.office_floor?.room_over_12_people}
        type="package_office"
        dict={dict}
        questionText={
          dict?.office_floor?.if_you_need_an_office_that_is_fully_furnished_and
        }
      />
    </section>
  );
}
