import React from 'react'
import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

export default function LoadingPage() {
     // loading icon
  const antIcon = (
    <LoadingOutlined
      style={{
        fontSize: 24,
      }}
      spin
    />
  );

  return (
    <div id='loadingPage' className='w-[100vw] h-[100vh] bg-[rgba(0,0,0,0.6)] flex items-center justify-center fixed top-0 left-0 z-[100]'>
         {/* Loading page */}
     <Spin indicator={antIcon} className='text-orange'/>
    </div>
  )
}
