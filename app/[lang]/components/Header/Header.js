"use client";
import Image from "next/image";
import React, { useEffect, useRef, useState } from "react";
import Logo from "../../../../public/img/img-header/Logo_FOffice_v3 1.png";
import vie from "../../../../public/img/flat/vie.png";
import us from "../../../../public/img/flat/en.png";
import "../../../../css/header.css";
import { FaPhoneAlt } from "react-icons/fa";
import { FiArrowRightCircle } from "react-icons/fi";
import Link from "next/link";
import { HiMenuAlt1, HiX } from "react-icons/hi";
import { usePathname } from "next/navigation";
import { getDictionary } from "../../distionaries";
import { getLanguage, langugeService } from "@/service/languageService";
import { setLanguageEn, setLanguageVi } from "@/app/redux/slice/languageSlice";

export default function Header({ lang }) {
  const [headerData, setHeaderData] = useState({});
  const { type, general, setting } = headerData;
  const navRef = useRef();
  const pathname = usePathname();
  const showNavbar = () => {
    navRef.current.classList.toggle("responsive_nav");
  };
  // Update ngôn ngữ từ redux
  const getLanguageAPI = async () => {
    let res = await langugeService.getLanguage();
    // console.log(res);
    if (res.data.success) {
      setLanguageVi(res.data.data.vi);
      setLanguageEn(res.data.data.en);
    }
  };

  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setHeaderData(res);
    }
  };

  useEffect(() => {
    getLanguageAPI().then(() => {
      getDict(lang);
    });
  }, [lang]);

  const renderPathname = () => {
    const path = pathname.split("/");
    path.splice(0, 1);
    if (lang === "en") {
      path[0] = "vi";
    } else {
      path[0] = "en";
    }
    const pathNew = `/${path.join("/")}`;
    return pathNew;
  };

  return (
    <header className="bg-white">
      <div className="container-2 flex flex-wrap items-center justify-between mx-auto header-container max-h-max">
        <Link href={`/${lang}/`} className="logo">
          <Image src={Logo} alt="Logo Felix Office" />
        </Link>
        <nav ref={navRef} className="flex lg:items-center">
          <button className="nav-btn nav-close-btn " onClick={showNavbar}>
            <HiX />
          </button>
          <Link
            href={`/${lang}/san-van-phong`}
            className={
              pathname.includes(`/${lang}/san-van-phong`)
                ? "text-orange uppercase lg:pl-4 xl:px-3.5"
                : "uppercase lg:pl-4 xl:px-3.5"
            }
          >
            {type?.officeFloor}
            <span className="lg:hidden">
              <FiArrowRightCircle />
            </span>
          </Link>
          <Link
            href={`/${lang}/van-phong-tron-goi`}
            className={
              pathname == "/van-phong-tron-goi"
                ? "text-orange uppercase lg:pl-4 xl:px-3.5"
                : "uppercase lg:pl-4 xl:px-3.5"
            }
          >
            {type?.allInclusiveOffice}
            <span className="lg:hidden">
              <FiArrowRightCircle />
            </span>
          </Link>
          <Link
            href={`/${lang}/toa-nha`}
            className={
              pathname == "/toa-nha"
                ? "text-orange uppercase lg:pl-4 xl:px-3.5"
                : "uppercase lg:pl-4 xl:px-3.5"
            }
          >
            {general?.officeBuildingShort}
            <span className="lg:hidden">
              <FiArrowRightCircle />
            </span>
          </Link>
          <Link
            href={`/${lang}/mat-bang-kinh-doanh`}
            className={
              pathname == "/mat-bang-kinh-doanh"
                ? "text-orange uppercase lg:pl-4 xl:px-3.5"
                : "uppercase lg:pl-4 xl:px-3.5"
            }
          >
            {type?.businessPremises}
            <span className="lg:hidden">
              <FiArrowRightCircle />
            </span>
          </Link>
          <Link
            href={`/${lang}/ban`}
            className={
              pathname == "/ban"
                ? "text-orange uppercase lg:pl-4 xl:px-3.5"
                : "uppercase lg:pl-4 xl:px-3.5"
            }
          >
            {general?.sell}
            <span className="lg:hidden">
              <FiArrowRightCircle />
            </span>
          </Link>
          <Link
            href={`/${lang}/lien-he`}
            className={
              pathname == "/lien-he"
                ? "text-orange uppercase lg:pl-4 xl:px-3.5"
                : "uppercase lg:pl-4 xl:px-3.5"
            }
          >
            {general?.contact}
            <span className="lg:hidden">
              <FiArrowRightCircle />
            </span>
          </Link>
          <a
            href={`tel:${setting?.hotline}`}
            className="hidden lg:inline-block"
          >
            <div className="flex items-center px-2 space-x-1.5">
              <span className="phone-icon hidden lg:inline-block ">
                <FaPhoneAlt />
              </span>
              <span className="phone-text ">
                <span className="hidden xl:inline">Hotline:</span>{" "}
                {setting?.hotline}
              </span>
            </div>
          </a>
          <a className="phoneMobile lg:hidden space-y-2" href="tel:090944096">
            <span className="phoneMobileText">
              {general?.help_you_quickly_find_the_right_office_and_space}
            </span>
            <span className="phoneMobileText2">{setting?.hotline}</span>
          </a>
          <Link href={renderPathname()}>
            <Image src={lang === "en" ? vie : us} alt="Đối tác" width={23} />
          </Link>
          {/* Đổi ngôn ngữ */}
        </nav>
        <button className="nav-btn" onClick={showNavbar}>
          <HiMenuAlt1 />
        </button>
      </div>
    </header>
  );
}
