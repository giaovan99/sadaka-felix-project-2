import React from "react";
import "../../../../../css/LienHeSearchPage.css";
import FormLienHe from "../../FormLienHe/FormLienHe";
export default function LienHeSearchPage({ dict }) {
  return (
    <div className="LienHeSearchPage bg-white pt-16 pb-16 lg:pb-52">
      <div className="container-2 space-y-4 lg:space-y-5">
        <div className="title">
          <p className="title--section--4">
            {dict?.general?.not_found_product}
          </p>
        </div>
        <div className="text">
          <p>{dict?.general?.let_our_consultants_help_you_find_the_most}</p>
        </div>
        <div className="contact space-y-3 lg:space-y-0">
          <a
            href={`tel:${dict?.setting?.hotline}`}
            className="text-orange underline "
          >
            {dict?.setting?.hotline}
          </a>
          <br className="lg:hidden" />{" "}
          <span className="mt-3 lg:mt-0 inline-block">
            {" "}
            &nbsp;{dict?.general?.or}
          </span>{" "}
          <br className="lg:hidden" />{" "}
          <FormLienHe text={dict?.general?.contact_us} dict={dict} />
        </div>
      </div>
    </div>
  );
}
