"use client";
import React from "react";
import "../../../../../css/TitleSearchPage.css";
import { usePathname, useRouter } from "next/navigation";

export default function TitleSearchPage({ type, dict }) {
  const router = useRouter();
  const pathname = usePathname();
  return (
    <div className="TitleSearchPage py-6 lg:py-12 space-y-3 lg:space-y-8">
      <div className="title text-center ">
        <h4 className="title--section--1 font-semibold lg:font-normal">
          {dict?.general?.search}
        </h4>
      </div>
      <div className="refine-result flex justify-center">
        <button
          className="main-button px-12"
          onClick={() => {
            if (pathname.includes("/ban")) {
              return router.push(`/${pathname.split("/")[1]}/ban`);
            } else {
              if (type == "office_floor") {
                router.push(`/${pathname.split("/")[1]}/san-van-phong`);
              } else if (type == "building") {
                router.push(`/${pathname.split("/")[1]}/toa-nha`);
              } else {
                router.push(`/${pathname.split("/")[1]}/mat-bang-kinh-doanh`);
              }
            }
          }}
        >
          {dict?.general?.find_again}
        </button>
      </div>
    </div>
  );
}
