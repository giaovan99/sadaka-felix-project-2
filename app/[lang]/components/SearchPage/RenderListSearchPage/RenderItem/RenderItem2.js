"use client";
import React, { useEffect, useState } from "react";
import { MdOutlineZoomOutMap } from "react-icons/md";
import { HiLocationMarker } from "react-icons/hi";
import { usePathname, useRouter } from "next/navigation";
import { Modal } from "antd";

export default function RenderItem2({ item, estates, dict }) {
  const [type, setType] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  let router = useRouter();
  const pathname = usePathname();
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  useEffect(() => {
    switch (item.type) {
      case "office_floor":
        setType(dict?.general?.office_floors);
        break;
      case "building":
        setType(dict?.general?.office_building);
        break;
      case "package_office":
        setType(dict?.general?.all_inclusive_office);
        break;
      case "business_premises":
        setType(dict?.general?.business_premises);
        break;
    }
  }, []);

  let turnToDetail = () => {
    if (pathname.includes("/ban")) {
      router.push(`/${pathname.split("/")[1]}/chi-tiet-ban/${item.slug}`);
    } else {
      router.push(`/${pathname.split("/")[1]}/chi-tiet/${item.slug}`);
    }
  };

  // console.log(item);

  return (
    <div className="RenderItem RenderItem2 p-5 lg:p-3 overflow-hidden">
      <div className="detail space-y-4 block">
        {/* img */}
        <div className="img h-[450px]" onClick={turnToDetail}>
          <img src={item.image} alt="..." className="h-full" />
        </div>
        {/* name*/}
        <div
          className="detail-info flex justify-between items-start"
          onClick={turnToDetail}
        >
          <p className="name line-clamp-1">{item.name}</p>
          <p className="detail-icon flex lg:mt-[-5px]">
            <HiLocationMarker />
          </p>
        </div>

        {/* detail */}
        <div
          className="detail-info space-y-2 lg:space-y-3"
          onClick={turnToDetail}
        >
          {/* location */}
          <div className="grid grid-cols-3	">
            <p className="sub-title"> {dict?.general?.address}</p>
            <p className="disc col-span-2  line-clamp-1">{item.address}</p>
          </div>

          {/* price */}
          <div className="grid grid-cols-3">
            <p className="sub-title">{dict?.general?.price} </p>
            <p
              className="disc col-span-2"
              dangerouslySetInnerHTML={{ __html: item.price_format }}
            ></p>
          </div>

          {/* space */}
          <div className="grid grid-cols-3 pb-5">
            <p className="sub-title">{dict?.general?.acreage}</p>
            <p className="disc col-span-2 line-clamp-1">
              {!item.area_floor ? (
                <span>{dict?.estate?.updating}</span>
              ) : (
                <span>{item.area_floor}</span>
              )}
            </p>
          </div>
        </div>

        {/* map */}
        <div
          className="map hidden lg:block"
          style={{
            height: `100px`,
            margin: `auto`,
            border: "none",
          }}
          onClick={showModal}
        >
          <div className="overlay space-y-2">
            <p>{dict?.general?.view_on_the_map}</p>
            <MdOutlineZoomOutMap />
          </div>
        </div>
        {/* Modal */}
        <Modal
          title={item.name}
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          centered={true}
          bodyStyle={{ width: "max-content" }}
        >
          <div dangerouslySetInnerHTML={{ __html: item.link_google_map }} />
        </Modal>
      </div>
    </div>
  );
}
