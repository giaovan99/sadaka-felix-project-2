"use client";
import React, { useEffect, useState } from "react";
import { HiLocationMarker } from "react-icons/hi";
import { MdOutlineZoomOutMap } from "react-icons/md";
import { usePathname, useRouter } from "next/navigation";
import { Modal } from "antd";
import Link from "next/link";

export default function RenderItem({ item, estates, dict }) {
  const [type, setType] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const pathname = usePathname();
  let router = useRouter();
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    switch (item.type) {
      case "office_floor":
        setType(dict?.general?.office_floors);
        break;
      case "building":
        setType(dict?.general?.office_building);
        break;
      case "package_office":
        setType(dict?.general?.all_inclusive_office);
        break;
      case "business_premises":
        setType(dict?.general?.business_premises);
        break;
    }
  }, []);

  let turnToDetail = () => {
    if (estates == "estates_sell") {
      router.push(`/${pathname.split("/")[1]}/chi-tiet-ban/${item.slug}`);
    } else {
      router.push(`/${pathname.split("/")[1]}/chi-tiet/${item.slug}`);
    }
  };
  let turnToDetail2 = () => {
    if (estates == "estates_sell") {
      return `/${pathname.split("/")[1]}/chi-tiet-ban/${item.slug}`;
    } else {
      return `/${pathname.split("/")[1]}/chi-tiet/${item.slug}`;
    }
  };

  if (item) {
    return (
      <div className="RenderItem block">
        <div className="grid grid-cols-1 lg:grid-cols-6 border-b-2 border-[#949494] pt-5 lg:pt-0">
          {/* Img */}
          <Link className="img w-full" href={turnToDetail2()}>
            <img src={item.image} alt="..." />
          </Link>
          {/* Detail */}
          <div
            className="detail lg:pl-5 lg:col-span-4 flex flex-row-reverse justify-between lg:justify-normal lg:flex-row py-5 lg:py-9 lg:space-x-3"
            onClick={turnToDetail}
          >
            {/* icon */}
            <div className="detail-icon">
              <HiLocationMarker />
            </div>
            {/* info */}
            <div className="detail-info flex-auto grid grid-cols-2 space-y-2">
              {/* name */}
              <div className=" col-span-2  h-max">
                <p className="name line-clamp-1">{item.name}</p>
              </div>
              <div className="right-side space-y-2 col-span-2 ">
                {/* location*/}
                <div className=" grid grid-cols-6 lg:grid-cols-4 gap-2 roboto-font">
                  <div className="sub-title col-span-2 lg:col-span-1">
                    {dict?.general?.address}
                  </div>
                  <div className="disc col-span-4 lg:col-span-3  line-clamp-1">
                    {item.address}
                  </div>
                </div>

                {/* Avg price*/}
                <div className=" grid grid-cols-6 lg:grid-cols-4 gap-2 roboto-font">
                  <div className="sub-title col-span-2 lg:col-span-1">
                    {dict?.general?.price}
                  </div>
                  <div
                    className="disc col-span-4 lg:col-span-3"
                    dangerouslySetInnerHTML={{ __html: item.price_format }}
                  ></div>
                </div>

                {/* space */}
                <div className=" grid grid-cols-6 lg:grid-cols-4 gap-2 roboto-font">
                  <div className="sub-title col-span-2 lg:col-span-1">
                    {dict?.general?.acreage}
                  </div>
                  <div className="disc col-span-4 lg:col-span-3 line-clamp-1">
                    {!item.area_floor ? (
                      <>{dict?.estate?.updating}</>
                    ) : (
                      <>{item.area_floor}</>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Map */}
          <div className="map hidden lg:block" onClick={showModal}>
            <div className="overlay space-y-2">
              <p>{dict?.general?.view_on_the_map}</p>
              <MdOutlineZoomOutMap />
            </div>
          </div>
          {/* Modal */}
          <Modal
            title={item.name}
            open={isModalOpen}
            onOk={handleOk}
            onCancel={handleCancel}
            centered={true}
            bodyStyle={{ width: "max-content" }}
          >
            <div dangerouslySetInnerHTML={{ __html: item.link_google_map }} />
          </Modal>
        </div>
      </div>
    );
  }
}
