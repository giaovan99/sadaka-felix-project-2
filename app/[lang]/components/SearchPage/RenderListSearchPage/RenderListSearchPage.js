"use client";
import "../../../../../css/RenderListSearchPage.css";
import React, { useEffect, useState } from "react";

import RenderItem from "./RenderItem/RenderItem";
import RenderItem2 from "./RenderItem/RenderItem2";
import { TfiLayoutListThumbAlt, TfiLayoutGrid3Alt } from "react-icons/tfi";
import { Pagination } from "antd";
import { usePathname, useSearchParams } from "next/navigation";
import { useRouter } from "next/navigation";

export default function RenderListSearchPage({
  arrListProduct,
  // setPage,
  total,
  page,
  estates = null,
  dict,
}) {
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const router = useRouter();
  const [typeofView, setTypeofView] = useState(true);

  // funtions
  let onChange = (page) => {
    const params = new URLSearchParams(searchParams);
    params.set("page", page);
    // router.push(`${pathname}?${params.toString()}`);
    window.location.assign(`${pathname}?${params.toString()}`);
  };

  let addActive = () => {
    // Get the container element
    var btnContainer = document.getElementById("myDIV");

    // Get all buttons with class="btn" inside the container
    var btns = btnContainer.getElementsByClassName("btn");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("active");

        // If there's no active class
        if (current.length > 0) {
          current[0].className = current[0].className.replace(" active", "");
        }

        // Add the active class to the current/clicked button
        this.className += " active";
      });
    }
  };

  useEffect(() => {
    addActive();
  }, [typeofView]);

  let renderItem = () => {
    return arrListProduct.map((item, index) => {
      return (
        <RenderItem item={item} key={index} estates={estates} dict={dict} />
      );
    });
  };

  let renderItem2 = () => {
    return arrListProduct.map((item, index) => {
      return (
        <RenderItem2 item={item} key={index} estates={estates} dict={dict} />
      );
    });
  };

  return (
    <div className="RenderListSearchPage pt-6 lg:pt-12 space-y-12">
      <div className="title container-2 text-center lg:text-left flex justify-between">
        <p>
          {" "}
          {total} {dict?.general?.results}
        </p>
        <p id="myDIV">
          <span
            className="btn inline-block"
            onClick={() => {
              setTypeofView(false);
              addActive();
            }}
          >
            <TfiLayoutGrid3Alt />
          </span>
          <span
            className="btn  inline-block active"
            onClick={() => {
              setTypeofView(true);
              addActive();
            }}
          >
            <TfiLayoutListThumbAlt />
          </span>
        </p>
      </div>
      {typeofView && (
        <div className="render container-2 2xl:container-1">{renderItem()}</div>
      )}
      {!typeofView && (
        <div className="render  container-2 2xl:container-1">
          <div className="grid grid-cols-2 lg:grid-cols-3 mx-[-12px]">
            {renderItem2()}
          </div>
        </div>
      )}
      <div className="paginationCustom flex justify-center">
        <Pagination
          total={total}
          onChange={onChange}
          defaultPageSize={9}
          current={page ? Number(page) : 1}
          showSizeChanger={false}
        />
      </div>
    </div>
  );
}
