"use client";
import React, { useEffect, useRef, useState } from "react";
import "../../../../css/SelectComponent.css";
import { IoIosArrowUp } from "react-icons/io";
import { Input } from "antd";

export default function SelectAreaComponent({
  minSpace,
  maxSpace,
  selected,
  setMinSpace,
  setMaxSpace,
}) {
  const [isActive, setIsActive] = useState(false);
  let menuRef = useRef();
  useEffect(() => {
    let handler = (e) => {
      if (!menuRef.current.contains(e.target)) {
        setIsActive(false);
      }
    };
    document.addEventListener("mousedown", handler);
    return () => {
      document.removeEventListener("mousedown", handler);
    };
  }, []);

  return (
    <div className="SelectComponent SelectRange SelectSpace ">
      <div className="dropdown" ref={menuRef}>
        <div
          className="dropdown-btn pl-[10px] flex justify-between items-center"
          onClick={(e) => setIsActive(!isActive)}
        >
          <p>{selected}</p>
        </div>

        {isActive && (
          <div className="dropdown-content space-y-3">
            {/* title */}
            <div className="titleRange">
              <p>Chọn diện tích</p>
            </div>
            {/* Chọn khoảng range */}
            <div className="priceRangeInput flex items-center justify-between">
              <div className="pr-4">
                <p>Từ</p>
                <Input
                  type="number"
                  className="field input-min font-semibold"
                  id="spaceMin"
                  value={minSpace}
                  min={0}
                  step={1}
                  onChange={(e) => {
                    setMinSpace(Math.abs(e.target.value));
                  }}
                />
              </div>
              <div className="separator mt-6"></div>
              <div className="pl-4">
                <p>Đến</p>
                <Input
                  type="number"
                  className="field input-min font-semibold "
                  id="spaceMax"
                  value={maxSpace}
                  min={0}
                  step={1}
                  onChange={(e) => {
                    setMaxSpace(Math.abs(e.target.value));
                  }}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
