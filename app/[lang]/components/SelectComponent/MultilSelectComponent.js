import React, { useEffect, useState } from "react";
import { Select, Space } from "antd";
import "../../../../css/SelectComponent.css";

export default function MultilSelectComponent({
  arrayOptions,
  listDistricActive,
  setListDistricActive,
  dict,
}) {
  const [options, setoptions] = useState([]);
  const [value, setValue] = useState();

  useEffect(() => {
    let arr = [];
    arrayOptions.map((item, index) => {
      arr.push({
        label: item.type + " " + item.name,
        value: item.id,
      });
    });
    setoptions(arr);
  }, [arrayOptions]);

  useEffect(() => {
    setValue(listDistricActive);
  }, [listDistricActive]);

  const selectProps = {
    mode: "multiple",
    style: {
      width: "300px",
    },
    value,
    options,
    onChange: (newValue) => {
      setValue(newValue);
      setListDistricActive(newValue);
    },
    placeholder: dict?.general?.choose_districts,
    maxTagCount: "responsive",
  };

  return (
    <div className="mb-5 multiSelectComponent">
      <Space
        direction="vertical"
        style={{
          width: "100%",
        }}
      >
        <Select {...selectProps} />
      </Space>
    </div>
  );
}
