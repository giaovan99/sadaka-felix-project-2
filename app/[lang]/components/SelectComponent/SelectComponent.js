"use client";
import React, { useEffect, useRef, useState } from "react";
import "../../../../css/SelectComponent.css";
import { IoIosArrowUp } from "react-icons/io";

export default function SelectComponent({ selected, setSelected, options }) {
  const [isActive, setIsActive] = useState(false);
  let menuRef = useRef();
  useEffect(() => {
    let handler = (e) => {
      if (!menuRef.current.contains(e.target)) {
        setIsActive(false);
      }
    };
    document.addEventListener("mousedown", handler);
    return () => {
      document.removeEventListener("mousedown", handler);
    };
  }, []);

  let renderOption = () => {
    return options.map((option, index) => {
      return (
        <div
          className="dropdown-item"
          onClick={(e) => {
            setSelected(option);
            setIsActive(false);
          }}
          key={index}
        >
          {option}
        </div>
      );
    });
  };

  return (
    <div className="SelectComponent">
      <div className="dropdown" ref={menuRef}>
        <div
          className="dropdown-btn pl-[10px] flex justify-between items-center"
          onClick={(e) => setIsActive(!isActive)}
        >
          <p>{selected}</p>
          <p className="mouseDownIcon rotate-180 lg:hidden px-2 ">
            <IoIosArrowUp className="w-8 h-8" />
          </p>
        </div>
        {isActive && <div className="dropdown-content">{renderOption()}</div>}
      </div>
    </div>
  );
}
