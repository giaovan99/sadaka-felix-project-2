"use client";
import React, { useEffect, useRef, useState } from "react";
import "../../../../css/SelectComponent.css";
import { usePathname } from "next/navigation";

export default function SelectRangeComponent({
  selected,
  setValue,
  data,
  unit,
  type,
  dict,
}) {
  const pathname = usePathname();
  const [isActive, setIsActive] = useState(false);
  let menuRef = useRef();
  useEffect(() => {
    let handler = (e) => {
      if (!menuRef.current.contains(e.target)) {
        setIsActive(false);
      }
    };
    document.addEventListener("mousedown", handler);
    return () => {
      document.removeEventListener("mousedown", handler);
    };
  }, []);

  useEffect(() => {
    let rangeInput = document.querySelectorAll(".range-input input");
    let progress = document.querySelector(".slider .progress");
    if (progress) {
      progress.style.left = (data.minPrice / rangeInput[0].max) * 100 + "%";
      progress.style.right =
        100 - (data.maxPrice / rangeInput[1].max) * 100 + "%";
    }
  }, [isActive]);

  return (
    <div className="SelectComponent SelectRange">
      <div className="dropdown" ref={menuRef}>
        <div
          className="dropdown-btn pl-[10px] flex justify-between items-center"
          onClick={(e) => setIsActive(!isActive)}
        >
          <p>{selected}</p>
        </div>

        {isActive && (
          <div className="dropdown-content space-y-7">
            {/* title */}
            <div className="titleRange">
              <p>
                {dict.general.pchoose_a_price_rangep}{" "}
                {pathname.includes("/ban") ? (
                  <>(tỷ đồng)</>
                ) : type == "office_floor" ? (
                  <>(USD)</>
                ) : (
                  <>(triệu đồng)</>
                )}
              </p>
            </div>

            {/* thanh kéo chọn */}
            <div>
              <div className="slider">
                <div className="progress"></div>
              </div>
              {type == "office_floor" ? (
                <>
                  <div className="range-input" onChange={setValue}>
                    <input
                      type="range"
                      className="range-min"
                      min="0"
                      max="100"
                      value={data.minPrice}
                    />
                    <input
                      type="range"
                      className="range-max"
                      min="0"
                      max="100"
                      value={data.maxPrice}
                    />
                  </div>
                </>
              ) : (
                <>
                  <div className="range-input" onChange={setValue}>
                    <input
                      type="range"
                      className="range-min"
                      min="0"
                      max={pathname.includes("/ban") ? "1000" : "1500"}
                      value={data.minPrice}
                    />
                    <input
                      type="range"
                      className="range-max"
                      min="0"
                      max={pathname.includes("/ban") ? "1000" : "1500"}
                      value={data.maxPrice}
                    />
                  </div>
                </>
              )}
            </div>

            {/* khung cao nhất, thấp nhất */}
            <div className="priceRangeInput flex items-center justify-between">
              <div className="field">
                <p>{dict.general.min}</p>
                <p className="input-min font-semibold">
                  {unit}
                  {data.minPrice.toLocaleString()}
                </p>
              </div>
              <div className="separator"></div>
              <div className="field ">
                <p>{dict.general.max}</p>
                <p className="input-max font-semibold">
                  {unit}
                  {data.maxPrice.toLocaleString()}
                </p>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
