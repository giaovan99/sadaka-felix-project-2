import React from "react";
import "../../../../../css/Item.css";
import Link from "next/link";
import { usePathname } from "next/navigation";
export default function Item({ item }) {
  const pathname = usePathname();

  let chiTietSanPham2 = () => {
    if (pathname.includes("/ban")) {
      return `/${pathname.split("/")[1]}/chi-tiet-ban/${item.slug}`;
    } else {
      return `/${pathname.split("/")[1]}/chi-tiet/${item.slug}`;
    }
  };
  return (
    <div className="Item roboto-font ">
      <div>
        <Link className="item-img" href={chiTietSanPham2()}>
          <img src={item.image} alt="..." loading="lazy" />
        </Link>
        <div className="content px-3 pt-3 pb-7">
          {/* Tên sản phẩm */}
          <div className="title">
            <p className="font-semibold line-clamp-1">{item.name}</p>
          </div>
          {/* Địa chỉ */}
          <div className="address mt-1 line-clamp-2	h-[46px]">
            <p>{item.address}</p>
          </div>
          {/* Giá */}
          <div className="price mt-3 md:mt-4">
            <p dangerouslySetInnerHTML={{ __html: item.price_format }} />
          </div>
        </div>
      </div>
    </div>
  );
}
