"use client";
import React from "react";
import "../../../../css/ListItem.css";
import Item from "./Item/Item";
import { Carousel } from "antd";
export default function ListItem({ arr, estates }) {
  var settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 1023.98,
        settings: {
          slidesToShow: 2.3,
          infinite: false,
          dots: false,
          swipeToSlide: true,
        },
      },
      {
        breakpoint: 768.98,
        settings: {
          slidesToShow: 1.4,
          infinite: false,
          dots: false,
          swipeToSlide: true,
        },
      },
    ],
  };
  return (
    <div className="ListItem">
      <Carousel {...settings} swipeToSlide draggable>
        {arr.map((item, index) => {
          return (
            <div className="px-3 py-6" key={index}>
              <Item item={item} estates={estates} />
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}
