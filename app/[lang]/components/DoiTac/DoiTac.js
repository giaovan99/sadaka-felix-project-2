import React from "react";
import "../../../../css/DoiTac.css";
import FormLienHe from "../FormLienHe/FormLienHe";

export default function DoiTac({ dict }) {
  const { general, setting } = dict;
  return (
    <section className="doiTacSection space-y-6 md:space-y-8">
      <div className="title--section--2">
        <h3 className="text-center">
          {general?.you_are_a_partner_you_have_products_and_want_to}{" "}
        </h3>
      </div>
      <div>
        <FormLienHe text={general?.button_contact_form} dict={dict} />
      </div>
      <div className="hot-line roboto-font">
        <a
          href={`tel:${setting?.hotline}`}
          className="title--section--4 font-bold"
        >
          <span className="lien-he-main-orange">HOTLINE: </span>
          <span className=" underline ">{setting?.hotline}</span>
        </a>
      </div>
    </section>
  );
}
