import React from "react";
import "../../../../../css/GanDayChiTietPage.css";
import ListItem from "../../ListItem/ListItem";

export default function GanDayChiTietPage({ arr, estates = null, dict }) {
  return (
    <div className="GanDayChiTietPage pb-[60px] lg:pb-[120px]">
      <div className="container-2 space-y-6 lg:space-y-10">
        <div className="title title--section--2">
          <h3 className="font-medium uppercase text-center">
            {dict?.estate?.near_search_place}
          </h3>
        </div>
        <div className="list-gan-day">
          <ListItem arr={arr} estates={estates} />
        </div>
      </div>
    </div>
  );
}
