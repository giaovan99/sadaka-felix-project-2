import React from "react";
import "../../../../../css/MapChiTietPage.css";
export default function MapChiTietPage({ item, dict }) {
  return (
    <div className="MapChiTietPage py-8 lg:py-16 space-y-6 lg:space-y-16">
      <div className="title title--section--2">
        <h3 className="uppercase text-center font-medium">
          {dict?.estate?.map}
        </h3>
      </div>
      <div className="container-2 map">
        <div dangerouslySetInnerHTML={{ __html: item.link_google_map }} />
      </div>
    </div>
  );
}
