import React from "react";
import "../../../../../css/InfoChiTietPage.css";
import { AiFillCheckCircle } from "react-icons/ai";
import FormLienHe from "../../FormLienHe/FormLienHe";
import ChuyenVien from "../../../../../public/img/img-chi-tiet/Group 120.png";
import Image from "next/image";
import {
  BiArea,
  BiCompass,
  BiHomeAlt,
  BiHomeAlt2,
  BiLayer,
  BiMaleFemale,
} from "react-icons/bi";
import { RxDimensions } from "react-icons/rx";
import { TbToiletPaper } from "react-icons/tb";

export default function InfoChiTietPageMuaBan({ item, dict }) {
  const { estate, setting } = dict;
  return (
    <div
      className={
        item?.gallery
          ? "InfoChiTietPage container-2 gap-5"
          : "InfoChiTietPage container-2 gap-5 my-5"
      }
    >
      <div className="title grid grid-cols-1 lg:grid-cols-3 space-y-5 lg:space-y-0">
        <div className="left-side">
          <div className="item-info flex lg:space-x-3">
            <div className="hidden lg:block item-info-icon text-orange">/</div>
            <div className="item-info-text lg:space-y-2.5">
              <p className="hidden lg:block title--section--5 font-medium">
                {item.name}
              </p>
              <p className="roboto-font w-[72%] md:w-full">{item.address}</p>
            </div>
          </div>
        </div>
        <div className="right-side lg:col-span-2 ">
          <p
            className="title-disc"
            dangerouslySetInnerHTML={{ __html: item.description }}
          ></p>
        </div>
      </div>
      <div className="detail grid grid-cols-1 lg:grid-cols-3 mt-3 lg:mt-16 relative space-y-10 lg:space-y-0">
        <div className="left-side roboto-font pr-[35px] 2xl:pr-[20%]">
          <div className="sticky-banner px-9 lg:px-3 py-6 xl:px-10 lg:py-10 sticky top-[110px]">
            <div className="info flex flex-col text-center space-y-3 lg:space-y-6">
              <div className="price">
                <p dangerouslySetInnerHTML={{ __html: item.price_format }}></p>
              </div>
              <div className="include space-y-2">
                {item.type == "office_floor" && (
                  <div className="include-item space-x-2">
                    <p className="icon">
                      <AiFillCheckCircle />
                    </p>
                    <p className="lg:text-[15px] xl:text-[16px]">
                      {estate?.management_fee_included}
                    </p>
                  </div>
                )}

                <div className="include-item space-x-2">
                  <p className="icon">
                    <AiFillCheckCircle />
                  </p>
                  <p className="lg:text-[15px] xl:text-[16px]">
                    {dict?.general?.good_faith_negotiation}
                  </p>
                </div>
              </div>
              <div className="hotline w-full text-center pt-3 lg:pt-0">
                <a
                  href={`tel:${setting?.hotline}`}
                  className="main-button w-full inline-block"
                >
                  {estate?.call}: {setting?.hotline}
                </a>
              </div>
              <div className="tu-van w-full">
                <FormLienHe
                  text={estate?.get_a_consultation_and_quote}
                  dict={dict}
                />
              </div>
              <div className="chuyen-vien flex flex-col justify-center items-center space-y-5 pt-3 lg:pt-0">
                <p>{estate?.consultants_ready_to_help}</p>
                <Image
                  src={ChuyenVien}
                  alt="Hinh anh chuyen vien"
                  height={60}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="right-side col-span-2 ">
          {/* item Thông số tòa nhà*/}
          <div className="item pb-10 md:pb-12">
            <div className="title">
              <p className="title--section--4 font-bold">
                {estate?.building_parameters}
              </p>
            </div>
            <div className="info flex flex-col md:grid md:grid-cols-3 space-y-5 md:space-y-0">
              {/* info hướng tòa nhà */}
              <div className="info-item space-x-4 	">
                <div className="icon">
                  <BiCompass />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.building_direction}</p>
                  <p>
                    {!item.building_direction
                      ? estate?.updating
                      : item.building_direction}
                  </p>
                </div>
              </div>
              {/* Kết cấu */}
              <div className="info-item space-x-4 	">
                <div className="icon">
                  <BiLayer />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.structure}</p>
                  <p>{!item.storey ? estate?.updating : item.storey}</p>
                </div>
              </div>
              {/* Số phòng */}
              <div className="info-item space-x-4 	">
                <div className="icon">
                  <BiHomeAlt />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.room_number}</p>
                  <p>
                    {!item.number_rooms ? estate?.updating : item.number_rooms}
                  </p>
                </div>
              </div>
              {/* Số toilet */}
              <div className="info-item space-x-4 	">
                <div className="icon">
                  {/* <BiMaleFemale /> */}
                  <TbToiletPaper />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.toilets_number}</p>
                  <p>
                    {!item.number_toliet
                      ? estate?.updating
                      : item.number_toliet}
                  </p>
                </div>
              </div>
              {/* Ngang x dài */}
              <div className="info-item space-x-4 	">
                <div className="icon">
                  <RxDimensions />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.width_x_length_m}</p>
                  <p>
                    {!item.wide_length ? (
                      estate?.updating
                    ) : (
                      <span>{item.wide_length}</span>
                    )}
                  </p>
                </div>
              </div>
              {/* Mặt hậu */}
              <div className="info-item space-x-4 	">
                <div className="icon">
                  <BiHomeAlt2 />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.back_side}</p>
                  <p>
                    {!item.back_side ? estate?.updating : item.back_side}
                    {item.back_side ? "m" : ""}
                  </p>
                </div>
              </div>

              {/* Diện tích thực tế */}
              <div className="info-item space-x-4 	">
                <div className="icon">
                  <BiArea />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.actual_area}</p>
                  <p>
                    {item?.area}m<sup>2</sup>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* Liên hệ dành cho tòa nhà nguyên căn và mặt bằng kinh doanh */}
          <div className="lienHeTNvaMBKD pt-12 border-t-2 border-gray-300 space-y-5">
            <FormLienHe
              text={estate?.contact_now_to_see_directy_2}
              dict={dict}
            />
            <div
              dangerouslySetInnerHTML={{
                __html:
                  estate?.if_you_want_to_see_more_similar_areas_in_the_same,
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
