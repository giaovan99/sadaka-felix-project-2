import React from "react";
import "../../../../../css/InfoChiTietPage.css";
import { AiFillCheckCircle } from "react-icons/ai";
import FormLienHe from "../../FormLienHe/FormLienHe";
import ChuyenVien from "../../../../../public/img/img-chi-tiet/Group 120.png";
import Image from "next/image";
import {
  TbArrowAutofitHeight,
  TbCalendarTime,
  TbAirConditioning,
  TbMotorbike,
  TbCar,
  TbFileInvoice,
} from "react-icons/tb";
import { MdOutlineElevator, MdOutlineAir } from "react-icons/md";
import { BiArea, BiCompass, BiMoney } from "react-icons/bi";
import { RiLightbulbFlashLine } from "react-icons/ri";
import { RxDimensions } from "react-icons/rx";

export default function InfoChiTietPage({ item, dict }) {
  const { estate, setting } = dict;
  let renderFloor = () => {
    return JSON.parse(item.rental_properties).map((item, index) => {
      return (
        <div className="floor flex space-x-3" key={index}>
          <p>Lầu {item.floor}</p>
          <p>
            {item.area}m<sup>2</sup>
          </p>
        </div>
      );
    });
  };

  return (
    <div
      className={
        item?.gallery
          ? "InfoChiTietPage container-2 gap-5"
          : "InfoChiTietPage container-2 gap-5 my-5"
      }
    >
      <div className="title grid grid-cols-1 lg:grid-cols-3 space-y-5 lg:space-y-0">
        <div className="left-side">
          <div className="item-info flex lg:space-x-3">
            <div className="hidden lg:block item-info-icon text-orange">/</div>
            <div className="item-info-text lg:space-y-2.5">
              <p className="hidden lg:block title--section--5 font-medium">
                {item.name}
              </p>
              <p className="roboto-font w-[72%] md:w-full">{item.address}</p>
            </div>
          </div>
        </div>
        <div className="right-side lg:col-span-2 ">
          <p
            className="title-disc"
            dangerouslySetInnerHTML={{ __html: item.description }}
          ></p>
        </div>
      </div>
      <div className="detail grid grid-cols-1 lg:grid-cols-3 mt-3 lg:mt-16 relative space-y-10 lg:space-y-0">
        <div className="left-side roboto-font pr-[35px] 2xl:pr-[20%]">
          <div className="sticky-banner px-9 lg:px-3 py-6 xl:px-10 lg:py-10 sticky top-[110px]">
            <div className="info flex flex-col text-center space-y-3 lg:space-y-6">
              <div className="price">
                <p dangerouslySetInnerHTML={{ __html: item.price_format }}></p>
              </div>
              <div className="include space-y-2">
                {item.type == "office_floor" && (
                  <div className="include-item space-x-2">
                    <p className="icon">
                      <AiFillCheckCircle />
                    </p>
                    <p className="lg:text-[15px] xl:text-[16px]">
                      {estate?.management_fee_included}
                    </p>
                  </div>
                )}

                <div className="include-item space-x-2">
                  <p className="icon">
                    <AiFillCheckCircle />
                  </p>
                  <p className="lg:text-[15px] xl:text-[16px]">
                    {item?.type === "office_floor"
                      ? estate?.rental_price_is_negotiable
                      : dict?.general?.good_faith_negotiation}
                  </p>
                </div>
              </div>
              <div className="hotline w-full text-center pt-3 lg:pt-0">
                <a
                  href={`tel:${setting?.hotline}`}
                  className="main-button w-full inline-block"
                >
                  {estate?.call}: {setting?.hotline}
                </a>
              </div>
              <div className="tu-van w-full">
                <FormLienHe
                  text={estate?.get_a_consultation_and_quote}
                  dict={dict}
                />
              </div>
              <div className="chuyen-vien flex flex-col justify-center items-center space-y-5 pt-3 lg:pt-0">
                <p>{estate?.consultants_ready_to_help}</p>
                <Image
                  src={ChuyenVien}
                  alt="Hinh anh chuyen vien"
                  height={60}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="right-side col-span-2 ">
          {/* item Thông số tòa nhà*/}
          <div className="item pb-10 md:pb-12">
            <div className="title">
              <p className="title--section--4 font-bold">
                {estate?.building_parameters}
              </p>
            </div>
            <div className="info flex flex-col md:grid md:grid-cols-3 space-y-5 md:space-y-0">
              {/* info quy mô */}
              <div className="info-item space-x-4 order-1	">
                <div className="icon">
                  <TbArrowAutofitHeight />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.scale}</p>
                  <p>{!item.storey ? estate?.updating : item.storey}</p>
                </div>
              </div>
              {/* info thang máy */}
              {(item.type == "building" ||
                item.type == "office_floor" ||
                !item.type) && (
                <div className="info-item space-x-4 order-4	">
                  <div className="icon">
                    <MdOutlineElevator />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.elevator_system}</p>
                    <p>{!item.elevator ? estate?.updating : item.elevator}</p>
                  </div>
                </div>
              )}
              {/* info giờ làm việc */}
              {item.type == "office_floor" && (
                <div className="info-item space-x-4 order-6	">
                  <div className="icon">
                    <TbCalendarTime />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.working_hours}</p>
                    <p>
                      {!item.working_time
                        ? estate?.updating
                        : item.working_time}
                    </p>
                  </div>
                </div>
              )}
              {/* info ngangxdai */}
              {(item.type == "building" ||
                item.type == "business_premises" ||
                !item.type) && (
                <div className="info-item space-x-4 order-6	">
                  <div className="icon">
                    <RxDimensions />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.width_x_length_m} </p>
                    <p>
                      {!item.wide_length ? (
                        estate?.updating
                      ) : (
                        <span>{item.wide_length}</span>
                      )}
                    </p>
                  </div>
                </div>
              )}
              {/* info tầng điển hình */}
              {item.type == "office_floor" && (
                <div className="info-item space-x-4 order-2	">
                  <div className="icon">
                    <BiArea />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.typical_floor}</p>
                    <p>
                      {!item.typical_floor
                        ? estate?.updating
                        : item.typical_floor}
                    </p>
                  </div>
                </div>
              )}
              {/* info tổng diện tích */}
              {(item.type == "building" ||
                item.type == "business_premises" ||
                !item.type) && (
                <div className="info-item space-x-4 order-2	">
                  <div className="icon">
                    <BiArea />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.total_area} </p>
                    <p>
                      {item.area}m<sup>2</sup>
                    </p>
                  </div>
                </div>
              )}
              {/* info hệ thống điều hòa */}
              {(item.type == "building" ||
                item.type == "office_floor" ||
                !item.type) && (
                <div className="info-item space-x-4 order-5	">
                  <div className="icon">
                    <TbAirConditioning />
                  </div>
                  <div className="text">
                    <p className="icon-title">
                      {estate?.air_conditioning_system}
                    </p>
                    <p>{item.air_conditioning_system}</p>
                  </div>
                </div>
              )}
              {/* giả item cho mặt bằng cho thuê */}
              {item.type == "business_premises" && (
                <div className="hidden lg:block info-item space-x-4 order-3	"></div>
              )}
              {/* info hướng tòa nhà */}
              <div className="info-item space-x-4 order-3	">
                <div className="icon">
                  <BiCompass />
                </div>
                <div className="text">
                  <p className="icon-title">{estate?.building_direction}</p>
                  <p>
                    {!item.building_direction
                      ? estate?.updating
                      : item.building_direction}
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* item Điều kiện hợp đồng*/}
          {item.type == "office_floor" && (
            <div className="item py-10 md:py-12 border-y-2 border-gray-300">
              <div className="title">
                <p className="title--section--4 font-bold">
                  {estate?.contract_conditions_and_other_costs}
                </p>
              </div>
              <div className="info flex flex-col md:grid md:grid-cols-3 space-y-5 md:space-y-0">
                {/* info  điện máy lạnh*/}
                <div className="info-item space-x-4 order-1	">
                  <div className="icon">
                    <MdOutlineAir />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.air_conditioner}</p>
                    <p>
                      {!item.fee_electric_ac
                        ? estate?.updating
                        : item.fee_electric_ac}
                    </p>
                  </div>
                </div>
                {/* info phí ngoài giờ */}
                <div className="info-item space-x-4 mb-5 order-3">
                  <div className="icon">
                    <BiMoney />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.overtime_fee}</p>
                    <p>
                      {!item.fee_overtime
                        ? estate?.updating
                        : item.fee_overtime}
                    </p>
                  </div>
                </div>
                {/* info phí gửi xe máy */}
                <div className="info-item space-x-4 mb-5 order-5">
                  <div className="icon">
                    <TbMotorbike />
                  </div>
                  <div className="text">
                    <p className="icon-title">
                      {estate?.motorbike_parking_fee}
                    </p>
                    <p>
                      {!item.fee_parking ? estate?.updating : item.fee_parking}
                    </p>
                  </div>
                </div>
                {/* info điện sử dụng */}
                <div className="info-item space-x-4 order-2">
                  <div className="icon">
                    <RiLightbulbFlashLine />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.electricity_usage} </p>
                    <p>
                      {!item.fee_electric
                        ? estate?.updating
                        : item.fee_electric}
                    </p>
                  </div>
                </div>
                {/* info phí gửi oto */}
                <div className="info-item space-x-4 order-4">
                  <div className="icon">
                    <TbCar />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.car_parking_fee}</p>
                    <p>
                      {!item.fee_car_packing
                        ? estate?.updating
                        : item.fee_car_packing}
                    </p>
                  </div>
                </div>
                {/* info thời hạn hợp đồng */}
                <div className="info-item space-x-4 order-6">
                  <div className="icon">
                    <TbFileInvoice />
                  </div>
                  <div className="text">
                    <p className="icon-title">{estate?.contract_duration}</p>
                    <p>
                      {!item.contract_term
                        ? estate?.updating
                        : item.contract_term}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          )}
          {/* item Diện tích cho thuê*/}
          {item.type == "office_floor" &&
            JSON.parse(item?.rental_properties).length != 0 && (
              <div className="item space-y-5 md:space-y-6 py-10 md:py-12">
                <div className="title">
                  <p className="title--section--4 font-bold">
                    {estate?.rental_area}
                  </p>
                </div>
                <div className="info-area roboto-font space-y-5 md:space-y-0">
                  {renderFloor()}
                </div>
              </div>
            )}

          {/* Liên hệ dành cho tòa nhà nguyên căn và mặt bằng kinh doanh */}
          {(item.type == "business_premises" ||
            item.type == "building" ||
            !item.type) && (
            <div className="lienHeTNvaMBKD pt-12 border-t-2 border-gray-300 space-y-5">
              <FormLienHe
                text={estate?.contact_now_to_see_directy_2}
                dict={dict}
              />
              <div
                dangerouslySetInnerHTML={{
                  __html:
                    estate?.if_you_want_to_see_more_similar_areas_in_the_same,
                }}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
