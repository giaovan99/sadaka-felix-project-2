"use client";
import React, { useEffect, useState } from "react";
import "../../../../../css/ImageChiTietPage.css";
import { Fancybox } from "@fancyapps/ui";
import "@fancyapps/ui/dist/fancybox/fancybox.css";
import { Carousel } from "antd";
import { GrLinkNext } from "react-icons/gr";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <GrLinkNext className="w-[50%] h-[50%]" />
    </div>
  );
}
function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <GrLinkNext className="w-[50%] h-[50%] rotate-180" />
    </div>
  );
}

export default function ImageChiTietPage({ gallery }) {
  let [imgGallery, setImgGallery] = useState([]);
  let [mainImg, setMainImg] = useState(null);
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1.6,
    slidesToScroll: 0.4,
    arrows: true,
    swipeToSlide: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1023.98,
        settings: {
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
        },
      },
    ],
  };

  useEffect(() => {
    // Initialize Fancybox
    Fancybox.bind('[data-fancybox="gallery"]', {
      // Tùy chọn khác nếu cần
      // Ở đây chúng ta chỉ cần tắt tính năng hash
      hash: false,
    });
  }, []);

  useEffect(() => {
    if (gallery) {
      // lấy gallery hình ảnh
      let cloneArr = [...gallery];
      setMainImg(cloneArr[0]);
      setImgGallery(cloneArr.slice(1));
    }
  }, [gallery]);

  // set up list ảnh phụ
  let renderGallery = () => {
    let arr = [];
    let arr2 = [];
    let phanThieu = 0;
    let phanNguyen = 0;
    let html = [];

    // xét trường hợp imgGallery=0
    if (imgGallery.length == 0) {
      arr = new Array(4).fill(mainImg);
      return renderListImg(arr);
    }

    // xét trường hợp imgGallery>4
    else if (imgGallery.length < 4) {
      arr = [...imgGallery];
      phanThieu = 4 - imgGallery.length;
      let newArr = new Array(phanThieu).fill(mainImg);
      arr = [...arr, ...newArr];
      return renderListImg(arr);
    }

    //xét trường hợp imgGallery=4
    else if (imgGallery.length == 4) {
      arr = [...imgGallery];
      return renderListImg(arr);
    }

    //xét trường hợp imgGallery>4
    else if (imgGallery.length > 4) {
      phanNguyen = Math.ceil(imgGallery.length / 4);
      phanThieu = 4 * phanNguyen - imgGallery.length;
      const newArr = new Array(phanThieu).fill(mainImg);
      arr = [...imgGallery, ...newArr];

      arr.forEach((item, index) => {
        if ((index + 1) % 4 != 0) {
          arr2.push(item);
        } else {
          arr2.push(item);
          html.push(renderListImg(arr2));
          arr2 = [];
        }
      });
      return html;
    }
  };

  // render list ảnh phụ
  let renderListImg = (array) => {
    return (
      <div className="img-2 w-full h-[300px] lg:h-[600px]">
        {array.map((item, index) => {
          return renderImg(item, index);
        })}
      </div>
    );
  };

  // render ảnh phụ
  let renderImg = (object, index) => {
    return (
      <a
        href={object ? object.image : ""}
        data-fancybox="gallery"
        data-caption="Single image"
        className="mb-[3px] lg:mb-[6px] pl-[7px] lg:pl-[14px]"
      >
        <img src={object?.image} alt="...." loading="lazy" />
      </a>
    );
  };

  return (
    <div className="ImageChiTietPage mb-3 lg:mb-[60px]">
      <div className="container-2 ">
        <Carousel {...settings}>
          {/* ảnh chính */}
          <div className="main-img w-full h-[300px] lg:h-[600px] mr-[14px]">
            {mainImg && (
              <a
                href={mainImg ? mainImg.image : ""}
                data-fancybox="gallery"
                data-caption="Single image"
              >
                <img
                  src={mainImg?.image}
                  alt="...."
                  loading="lazy"
                  className="mainImg"
                />
              </a>
            )}
          </div>

          {/* ảnh phụ */}
          {gallery && renderGallery()}
        </Carousel>
      </div>
    </div>
  );
}
