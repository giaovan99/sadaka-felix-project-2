import React from "react";
import "../../../../../css/DatLichChiTietPage.css";
import FormLienHe from "../../FormLienHe/FormLienHe";

export default function DatLichChiTietPage({ item, dict }) {
  const { estate, setting } = dict;
  return (
    <div className="DatLichChiTietPage pt-16 pb-[60px] lg:pb-[120px]">
      <div className="container-2 space-y-5">
        <FormLienHe text={estate?.schedule_a_viewing} dict={dict} />
        <div className="hotline">
          <a
            href={`tel:${setting?.hotline}`}
            className="uppercase roboto-font title--section--4"
          >
            <span className=" font-semibold lg:font-medium">
              {estate?.contact_now_to_see_directy}{" "}
            </span>
            <span className="text-orange underline">{setting?.hotline}</span>
          </a>
        </div>
        <div
          className="disc  lg:w-[52%] roboto-font"
          dangerouslySetInnerHTML={{
            __html: estate?.if_you_want_to_see_more_similar_areas_in_the_same,
          }}
        />
      </div>
    </div>
  );
}
