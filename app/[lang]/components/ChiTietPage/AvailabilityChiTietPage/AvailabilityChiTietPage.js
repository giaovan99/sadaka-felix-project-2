import React from "react";
import "../../../../../css/AvailabilityChiTietPage.css";

export default function AvailabilityChiTietPage({ item, quotation }) {
  return (
    <div className="AvailabilityChiTietPage">
      <div className="container-2 flex justify-center lg:flex-col items-center lg:items-start h-full w-fulls flex-wrap">
        {quotation && (
          <div className="w-full">
            <p
              className="mb-2 main-button px-10 m-auto max-w-max cursor-pointer "
              onClick={() => history.back()}
            >
              Trở về báo giá
            </p>
          </div>
        )}
        <div className="left-side">
          <h2 className="title--section--5 font-semibold">{item.name}</h2>
        </div>
      </div>
    </div>
  );
}
