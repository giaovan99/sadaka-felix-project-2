import SelectAreaComponent from "@/app/[lang]/components/SelectComponent/SelectAreaComponent";
import React, { useEffect, useState } from "react";

export default function SelectSpace({
  minSpace,
  maxSpace,
  setMinSpace,
  setMaxSpace,
  selected,
  setSelected,
  dict,
}) {
  useEffect(() => {
    if ((!minSpace && !maxSpace) || (minSpace == 0 && maxSpace == 0)) {
      setSelected(dict?.general?.all);
    } else if (!maxSpace) {
      setSelected(
        <>
          {dict?.general?.from} {minSpace}m<sup>2</sup>
        </>
      );
    } else {
      setSelected(
        <>
          {dict?.general?.from} {minSpace}m<sup>2</sup> {dict?.general?.to}{" "}
          {maxSpace}m<sup>2</sup>
        </>
      );
    }
  }, [minSpace, maxSpace, dict]);

  return (
    <div className="SelectSpace select-item">
      <div className="title">{dict?.general?.acreage} (&#13217;)</div>
      <div className="select-options">
        <SelectAreaComponent
          minSpace={minSpace}
          maxSpace={maxSpace}
          selected={selected}
          setMinSpace={setMinSpace}
          setMaxSpace={setMaxSpace}
        />
      </div>
    </div>
  );
}
