import SelectComponent from "@/app/[lang]/components/SelectComponent/SelectComponent";
import React, { useEffect, useState } from "react";

export default function SelectGia({ setPrice }) {
  const [selected, setSelected] = useState("Tất cả");
  const options = [
    "Từ cao đến thấp",
    "Từ thấp đến cao",
    "Được quan tâm",
    "Tất cả",
  ];
  useEffect(() => {
    setPrice(selected);
  }, [selected]);

  return (
    <div className="SelectGia select-item">
      <div className="title">Giá</div>
      <div className="select-options">
        <SelectComponent
          selected={selected}
          setSelected={setSelected}
          options={options}
        />
      </div>
    </div>
  );
}
