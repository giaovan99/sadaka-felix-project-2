import SelectComponent from "@/app/[lang]/components/SelectComponent/SelectComponent";
import React, { useEffect, useState } from "react";

export default function SelectHang({ setRate }) {
  const [selected, setSelected] = useState("Tất cả");
  const options = ["A", "B", "C", "Tất cả"];
  useEffect(() => {
    setRate(selected);
  }, [selected]);

  return (
    <div className="SelectHang select-item">
      <div className="title">Hạng</div>
      <div className="select-options">
        <SelectComponent
          selected={selected}
          setSelected={setSelected}
          options={options}
        />
      </div>
    </div>
  );
}
