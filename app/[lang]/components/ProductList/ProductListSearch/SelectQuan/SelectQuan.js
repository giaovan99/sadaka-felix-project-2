"use client";
import SelectComponent from "@/app/[lang]/components/SelectComponent/SelectComponent";
import React, { useEffect, useState } from "react";

export default function SelectQuan({ arr, setDistrictsId, dict }) {
  const [selected, setSelected] = useState();
  const [options, setOptions] = useState([]);
  const { general } = dict;

  // tạo array options
  useEffect(() => {
    if (arr.length > 0) {
      let clone = [];
      arr.map((item) => {
        clone.push(`${item.type} ${item.name}`);
      });
      setOptions(clone);
    }
  }, [arr]);

  // setDistrictsId khi selected thay đổi
  useEffect(() => {
    if (selected !== general?.select_search && general?.select_search) {
      let index = arr.findIndex(
        (item) => `${item.type} ${item.name}` == selected
      );
      let id = arr[index].id;
      setDistrictsId(id);
    }
  }, [selected]);

  useEffect(() => {
    if (!selected) {
      return setSelected(general?.select_search);
    }
  }, [dict]);

  return (
    <div className="SelectQuan select-item">
      <div className="title">{general?.district}</div>
      <div className="select-options">
        <SelectComponent
          selected={selected}
          setSelected={setSelected}
          options={options}
        />
      </div>
    </div>
  );
}
