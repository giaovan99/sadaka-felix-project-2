import SelectRangeComponent from "@/app/[lang]/components/SelectComponent/SelectRangeComponent";
import { usePathname } from "next/navigation";
import React, { useEffect, useState } from "react";

export default function SelectGia2({ data, setData, type, dict }) {
  const pathname = usePathname();
  const { general } = dict;
  let [selected, setSelected] = useState("$0 - $100");
  let [selectedVND, setSelectedVND] = useState("0 triệu - 1.5 tỷ");

  useEffect(() => {
    if (pathname.includes("/ban")) {
      setSelectedVND("0 tỷ - 1000 tỷ");
    } else {
      setSelectedVND("0 triệu - 1.5 tỷ");
    }
  }, [pathname]);

  let setValue = () => {
    let rangeInput = document.querySelectorAll(".range-input input");
    let progress = document.querySelector(".slider .progress");
    rangeInput.forEach((input) => {
      input.addEventListener("input", () => {
        let min = parseInt(rangeInput[0].value);
        let max = parseInt(rangeInput[1].value);
        if (min < max) {
          progress.style.left = (min / rangeInput[0].max) * 100 + "%";
          progress.style.right = 100 - (max / rangeInput[1].max) * 100 + "%";
          setData({ ...data, minPrice: min, maxPrice: max });
          if (pathname.includes("/ban")) {
            setSelectedVND(
              `${min.toLocaleString()} ${"tỷ"} - ${max.toLocaleString()} ${"tỷ"}`
            );
          } else if (type == "office_floor") {
            setSelected(`$${min}  - $${max}`);
          } else {
            setSelectedVND(
              `${min >= 1000 ? min / 1000 : min.toLocaleString()} ${
                min >= 1000 ? "tỷ" : "triệu"
              } - ${max >= 1000 ? max / 1000 : max.toLocaleString()} ${
                max >= 1000 ? "tỷ" : "triệu"
              }`
            );
          }
        }
      });
    });
  };

  return (
    <div className="SelectGia2 select-item">
      <div className="title">
        {general?.price}
        {pathname.includes("/ban") ? (
          <>(tỷ đồng)</>
        ) : type == "office_floor" ? (
          <> (USD/&#13217;)</>
        ) : (
          <> (triệu đồng)</>
        )}
      </div>
      <div className="select-options">
        {type == "office_floor" ? (
          <SelectRangeComponent
            selected={selected}
            data={data}
            setValue={setValue}
            unit="$"
            type={type}
            dict={dict}
          />
        ) : (
          <SelectRangeComponent
            selected={selectedVND}
            data={data}
            setValue={setValue}
            unit=""
            type={type}
            dict={dict}
          />
        )}
      </div>
    </div>
  );
}
