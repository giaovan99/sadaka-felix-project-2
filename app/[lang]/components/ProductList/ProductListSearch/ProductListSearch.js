"use client";
import React, { useEffect, useState } from "react";
import "../../../../../css/ProductListSearch.css";
import { FaSearchLocation } from "react-icons/fa";
import SelectQuan from "./SelectQuan/SelectQuan";
import { usePathname, useRouter } from "next/navigation";
import SelectGia2 from "./SelectGia2/SelectGia2";
import SelectSpace from "./SelectSpace/SelectSpace";
import { Modal } from "antd";

export default function ProductListSearch({
  type,
  districtsList,
  estates = null,
  dict,
}) {
  const { general } = dict;
  const pathname = usePathname();
  let router = useRouter();
  let [districtsId, setDistrictsId] = useState("");
  let [data, setData] = useState({
    minPrice: 0,
    maxPrice: 100,
  });
  let [dataVND, setDataVND] = useState({
    minPrice: 0,
    maxPrice: 1500,
  });
  let [minSpace, setMinSpace] = useState();
  let [maxSpace, setMaxSpace] = useState();
  let [selectedSpace, setSelectedSpace] = useState("Tất cả");

  useEffect(() => {
    if (pathname.includes("/ban")) {
      setDataVND({
        minPrice: 0,
        maxPrice: 1000,
      });
    } else {
      setDataVND({
        minPrice: 0,
        maxPrice: 1500,
      });
    }
  }, [pathname]);

  const error = () => {
    Modal.error({
      title: dict?.general?.reselect_the_area,
      titleFontSize: 24,
      bodyStyle: { fontSize: "18px !important" },
      content: (
        <p className="text-[18px]">
          {dict?.general?.the_maximum_area_cannot_be_less_than} {minSpace}.
        </p>
      ),
      centered: true,
      okButtonProps: {
        style: { backgroundColor: "red" },
      },
    });
  };

  let timKiem = () => {
    let check = true;
    // check
    if (maxSpace <= minSpace) {
      error();
      check = false;
    }

    if (check) {
      // lấy keywords
      let keywords = document.getElementById("keywords").value;
      if (selectedSpace == "Tất cả" || selectedSpace == "All") {
        if (estates) {
          router.push(
            `/${
              pathname.split("/")[1]
            }/ban/search?limit=9&page=1&keywords=${keywords}&type=${type}&min_unit_price=${
              type == "office_floor" ? data.minPrice : dataVND.minPrice
            }&max_unit_price=${
              type == "office_floor" ? data.maxPrice : dataVND.maxPrice
            }&district_id=${districtsId}`
          );
        } else {
          router.push(
            `/${
              pathname.split("/")[1]
            }/search?limit=9&page=1&keywords=${keywords}&type=${type}&min_unit_price=${
              type == "office_floor" ? data.minPrice : dataVND.minPrice
            }&max_unit_price=${
              type == "office_floor" ? data.maxPrice : dataVND.maxPrice
            }&district_id=${districtsId}`
          );
        }
      } else {
        if (estates) {
          router.push(
            `/${
              pathname.split("/")[1]
            }/ban/search?limit=9&page=1&keywords=${keywords}&type=${type}&min_unit_price=${
              type == "office_floor" ? data.minPrice : dataVND.minPrice
            }&max_unit_price=${
              type == "office_floor" ? data.maxPrice : dataVND.maxPrice
            }&district_id=${districtsId}&min_area=${minSpace}&max_area=${maxSpace}`
          );
        } else {
          router.push(
            `/${
              pathname.split("/")[1]
            }/search?limit=9&page=1&keywords=${keywords}&type=${type}&min_unit_price=${
              type == "office_floor" ? data.minPrice : dataVND.minPrice
            }&max_unit_price=${
              type == "office_floor" ? data.maxPrice : dataVND.maxPrice
            }&district_id=${districtsId}&min_area=${minSpace}&max_area=${maxSpace}`
          );
        }
      }
    }
  };
  return (
    <div className="ProductListSearch flex justify-center">
      <div className="search-frame space-y-9">
        <div className="title">
          <h3 className="text-center flex flex-col space-y-3 sm:space-y-0 sm:block">
            <span className="text-orange text-[32px] md:text-[40px]">
              {general?.quick_search}
            </span>{" "}
            <span>{general?.according_to_criteria}</span>
          </h3>
        </div>
        <div className="filter grid grid-cols-1 roboto-font">
          {/* Loc thep ten, ten duong, dia chi */}
          <div className="filter-text py-3.5 px-4 flex space-x-3">
            <div className="icon">
              <FaSearchLocation />
            </div>
            <div className="input w-full">
              <input
                placeholder={general?.search_by_building_street_name_or_address}
                className="w-full "
                id="keywords"
              ></input>
            </div>
          </div>

          <div className="grid grid-cols-1 space-y-3 lg:space-y-0 lg:grid-cols-12 lg:gap-4">
            {/* Quan */}
            <div className="lg:col-span-4">
              <SelectQuan
                arr={districtsList}
                setDistrictsId={setDistrictsId}
                dict={dict}
              />
            </div>

            {/* Giá 2*/}
            <div className="lg:col-span-4">
              {type == "office_floor" ? (
                <SelectGia2
                  data={data}
                  setData={setData}
                  type={type}
                  dict={dict}
                />
              ) : (
                <SelectGia2
                  data={dataVND}
                  setData={setDataVND}
                  type={type}
                  dict={dict}
                />
              )}
            </div>

            {/* Diện tích*/}
            <div className="lg:col-span-4">
              <SelectSpace
                minSpace={minSpace}
                maxSpace={maxSpace}
                setMinSpace={setMinSpace}
                setMaxSpace={setMaxSpace}
                setSelected={setSelectedSpace}
                selected={selectedSpace}
                dict={dict}
              />
            </div>

            {/* Tìm kiếm Button */}
            <div className="lg:col-span-12">
              <button
                className="main-button flex justify-center items-center leading-[15px] w-full h-full"
                onClick={timKiem}
              >
                {general?.search}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
