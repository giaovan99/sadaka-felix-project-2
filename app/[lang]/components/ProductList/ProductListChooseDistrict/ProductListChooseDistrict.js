"use client";
import React, { useState } from "react";
import "../../../../../css/ProductListChooseDistrict.css";
import ProductListChooseDistrictItem from "./ProductListChooseDistrictItem/ProductListChooseDistrictItem";
import SelectComponent from "../../SelectComponent/SelectComponent";
import { useEffect } from "react";
import MultilSelectComponent from "../../SelectComponent/MultilSelectComponent";
import LoadingPage from "../../LoadingPage/LoadingPage";

export default function ProductListChooseDistrict({
  arr,
  checkActiveDistrict,
  listDistricActive,
  setListDistricActive,
  result,
  setResult,
  loading,
  setLoading,
  dict,
}) {
  const [districtArr, setDistrictArr] = useState([]);
  const [districtArr2, setDistrictArr2] = useState([]);
  const [selected, setSelected] = useState("Quận");
  const { general } = dict;

  let renderList = () => {
    if (arr.length > 0) {
      return arr.map((item, index) => {
        return (
          <ProductListChooseDistrictItem
            checkActiveDistrict={checkActiveDistrict}
            listDistricActive={listDistricActive}
            item={item}
            key={index}
          />
        );
      });
    }
  };

  useEffect(() => {
    if (arr.length > 0) {
      let clone = [];
      let clone2 = [];
      arr.map((item) => {
        clone.push("Quận " + item.name);
        clone2.push(item);
      });
      setDistrictArr(clone);
      setDistrictArr2(clone2);
    }
  }, [arr]);

  useEffect(() => {
    setLoading(true);
    let index = districtArr.findIndex((item) => item == selected);
    if (index != -1) {
      setListDistricActive([districtArr2[index].id]);
    }
  }, [selected]);

  return (
    <div className="ProductListChooseDistrict">
      {/* Title */}
      <div className="container-2 mb-3">
        <h4 className="title text-[16px] md:text-[30px] font-semibold mb-4">
          {general?.select_the_displayed_district}
        </h4>
        <div className="select-options flex justify-between flex-wrap">
          {arr.length > 0 && (
            <MultilSelectComponent
              arrayOptions={arr}
              listDistricActive={listDistricActive}
              setListDistricActive={setListDistricActive}
              dict={dict}
            />
          )}

          {/* Hiển thị số kết quả */}
          <div className="md:text-xl">
            {general?.have}{" "}
            <span className="text-orange font-medium">{result}</span>{" "}
            {general?.results}
          </div>
        </div>
      </div>
      {/* Desktop view */}
      <div className="hidden container-2 lg:flex flex-wrap ">
        {renderList()}
      </div>
      {/* LoandingPage */}
      {loading && <LoadingPage />}
    </div>
  );
}
