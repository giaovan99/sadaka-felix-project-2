import React, { useState } from "react";

export default function ProductListChooseDistrictItem({
  item,
  checkActiveDistrict,
  listDistricActive,
}) {
  const checkActive = listDistricActive.findIndex((id) => id == item.id);

  return (
    <button
      onClick={() => {
        checkActiveDistrict(item.id);
      }}
      className="inline-block pr-5 mb-5"
    >
      <p
        className={`ProductListChooseDistrictItem ${
          checkActive !== -1 ? "active" : ""
        }`}
      >
        {item.type} {item.name}
      </p>
    </button>
  );
}
