import React from "react";
import "../../../../../css/ProductListHeader.css";

export default function ProductListHeader({ text }) {
  console.log(text);
  return (
    <div className="ProductListHeader">
      <h2 className="title--section--3">{text}</h2>
    </div>
  );
}
