import React from "react";
import ProductListRenderItem from "./Item/ProductListRenderItem";
import "../../../../../css/ProductListRender.css";

export default function ProductListRender({
  districts,
  type,
  estates = null,
  dict,
}) {
  let renderList = () => {
    return districts.map((item, index) => {
      return (
        <ProductListRenderItem
          key={index}
          district={item}
          type={type}
          estates={estates}
          dict={dict}
        />
      );
    });
  };
  return <div className="ProductListRender">{renderList()}</div>;
}
