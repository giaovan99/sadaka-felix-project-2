"use client";
import ListItem from "@/app/[lang]/components/ListItem/ListItem";
import { productService } from "@/service/productService";
import { sellService } from "@/service/sellService";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React, { useEffect, useState } from "react";

export default function ProductListRenderItem({
  district,
  type,
  estates,
  dict,
}) {
  const pathname = usePathname();
  const [arr, setArr] = useState([]);

  useEffect(() => {
    getListProduct();
  }, [district]);

  // lấy danh sách sản phẩm
  let getListProduct = async () => {
    let res;
    if (estates == "estates_sell") {
      res = await sellService.getProductSell({
        limit: 10,
        page: 1,
        type: type,
        keywords: "",
        district_id: district.id,
        min_price: "",
        max_price: "",
        sort_field: "",
        sort_order: "",
        interested: "",
        uid: "",
      });
    } else {
      res = await productService.getProduct({
        limit: 10,
        page: 1,
        type: type,
        keywords: "",
        district_id: district.id,
        min_price: "",
        max_price: "",
        sort_field: "",
        sort_order: "",
        interested: "",
        uid: "",
      });
    }
    // console.log(res);
    setArr(res.data.data);
    // console.log;
  };

  return (
    <div className="ProductListRenderItem pt-0 md:pt-16">
      <div className="container-2 space-y-0 md:space-y-9">
        <div className="title flex justify-between items-center">
          <div className="title-location">
            <h4>
              {pathname.includes("/ban")
                ? dict?.general?.sell
                : type == "office_floor"
                ? dict?.type?.officeFloor
                : type == "building"
                ? dict?.type?.officeBuilding
                : dict?.general?.business_premises}{" "}
              {pathname.split("/")[1] == "en" ? " in " : ""}
              {district.type} {district.name}
            </h4>
          </div>
          <div className="title-button">
            <button className="underline">
              <a
                href={
                  pathname.includes("/ban")
                    ? `/${
                        pathname.split("/")[1]
                      }/ban/search?type=${type}&district_id=${district.id}`
                    : `/${
                        pathname.split("/")[1]
                      }/search?type=${type}&district_id=${district.id}`
                }
              >
                {dict?.general?.see_more}
              </a>
            </button>
          </div>
        </div>
      </div>
      <div className="list-items md:block">
        <div className="container-2">{arr && <ListItem arr={arr} />}</div>
      </div>
    </div>
  );
}
