import React from "react";
import "../../../../../css/ProductListEndPage.css";
import Image from "next/image";
import Image1 from "../../../../../public/img/img-san-van-phong/Rectangle 137.jpg";
import Image2 from "../../../../../public/img/img-san-van-phong/Rectangle 138.jpg";
import Image3 from "../../../../../public/img/img-san-van-phong/Rectangle 139.jpg";
import mbSVP from "../../../../../public/img/hinh-up-web/mua-ban/SVP.jpg";
import mbMBKD from "../../../../../public/img/hinh-up-web/mua-ban/MBKD.jpg";
import mbTN from "../../../../../public/img/hinh-up-web/mua-ban/TN.jpg";
import mbkd1 from "../../../../../public/img/hinh-up-web/MBKD/mbkd1.jpg";
import mbkd2 from "../../../../../public/img/hinh-up-web/MBKD/mbkd2.jpg";
import mbkd3 from "../../../../../public/img/hinh-up-web/MBKD/mbkd3.jpg";
import { usePathname } from "next/navigation";
export default function ProductListEndPage({
  title,
  text1,
  text2,
  text3,
  type,
  questionText,
}) {
  let pathname = usePathname();
  return (
    <div className="ProductListEndPage py-8  lg:py-16">
      <div className="container-1 ">
        <div className="title mb-5 lg:mb-6">
          <h3 className="title--section--1 font-semibold">{title}</h3>
        </div>
        <div className="disc roboto-font mb-10 lg:mb-16">
          <p>{questionText}</p>
        </div>
        <div className="img grid grid-cols-1 lg:grid-cols-3  gap-10 lg:gap-16 roboto-font">
          {/* item */}
          <div className="item">
            {type == "sell" ? (
              // Dẫn đến trang "Sàn văn phòng"
              <a href="/san-van-phong">
                <div className="item-img">
                  <Image src={mbSVP} alt="Sàn văn phòng" />
                </div>
                <div className="item-text">{text1}</div>
              </a>
            ) : type == "package_office" ? (
              // Dẫn đến trang "Văn phòng trọn gói"
              <a href="/van-phong-tron-goi">
                <div className="item-img">
                  <Image src={Image1} alt="Văn phòng trọn gói" />
                </div>
                <div className="item-text">{text1}</div>
              </a>
            ) : type == "office_floor" ? (
              // Dẫn đến trang search danh sách sản phẩm "Sàn văn phòng" diện tích 500 -> 1000
              <a
                href={`/${
                  pathname.split("/")[1]
                }/search?type=${type}&min_area=${500}&max_area=${1000}`}
              >
                <div className="item-img">
                  <Image src={Image1} alt="Sàn văn phòng" />
                </div>
                <div className="item-text">{text1}</div>
              </a>
            ) : (
              // Dẫn đến trang search danh sách sản phẩm "Tòa nhà nguyên căn" diện tích 300 -> 500
              <a
                href={`/${
                  pathname.split("/")[1]
                }/search?type=${type}&min_area=${300}&max_area=${500}`}
              >
                <div className="item-img">
                  <Image src={mbkd1} alt="Tòa nhà nguyên căn" />
                </div>
                <div className="item-text">{text1}</div>
              </a>
            )}
          </div>
          {/* item */}
          <div className="item">
            {type == "sell" ? (
              // Dẫn đến trang "Toà nhà"
              <a href={`/${pathname.split("/")[1]}/toa-nha`}>
                <div className="item-img">
                  <Image src={mbTN} alt="Toà nhà" />
                </div>
                <div className="item-text">{text2}</div>
              </a>
            ) : type == "package_office" ? (
              // Dẫn đến trang "Văn phòng trọn gói"
              <a href={`/${pathname.split("/")[1]}/van-phong-tron-goi`}>
                <div className="item-img">
                  <Image src={Image2} alt="Văn phòng trọn gói" />
                </div>
                <div className="item-text">{text2}</div>
              </a>
            ) : type == "office_floor" ? (
              // Dẫn đến trang search danh sách sản phẩm "Sàn văn phòng" diện tích 1000 -> 1500
              <a
                href={`/${
                  pathname.split("/")[1]
                }/search?type=${type}&min_area=${1000}&max_area=${1500}`}
              >
                <div className="item-img">
                  <Image src={Image2} alt="Sàn văn phòng" />
                </div>
                <div className="item-text">{text2}</div>
              </a>
            ) : (
              // Dẫn đến trang search danh sách sản phẩm "Tòa nhà nguyên căn" diện tích 500 -> 1000
              <a
                href={`/${
                  pathname.split("/")[1]
                }/search?type=${type}&min_area=${500}&max_area=${1000}`}
              >
                <div className="item-img">
                  <Image src={mbkd2} alt="Tòa nhà nguyên căn" />
                </div>
                <div className="item-text">{text2}</div>
              </a>
            )}
          </div>
          {/* item */}
          <div className="item">
            {type == "sell" ? (
              // Dẫn đến trang "Mặt bằng kinh doanh"
              <a href={`/${pathname.split("/")[1]}/mat-bang-kinh-doanh`}>
                <div className="item-img">
                  <Image src={mbMBKD} alt="Mặt bằng kinh doanh" />
                </div>
                <div className="item-text">{text3}</div>
              </a>
            ) : type == "package_office" ? (
              // Dẫn đến trang "Văn phòng trọn gói"
              <a href={`/${pathname.split("/")[1]}/van-phong-tron-goi`}>
                <div className="item-img">
                  <Image src={Image3} alt="Văn phòng trọn gói" />
                </div>
                <div className="item-text">{text3}</div>
              </a>
            ) : type == "office_floor" ? (
              // Dẫn đến trang search danh sách sản phẩm "Sàn văn phòng" diện tích >1500
              <a
                href={`/${
                  pathname.split("/")[1]
                }/search?type=${type}&min_area=${1500}`}
              >
                <div className="item-img">
                  <Image src={Image3} alt="Sàn văn phòng" />
                </div>
                <div className="item-text">{text3}</div>
              </a>
            ) : (
              // Dẫn đến trang search danh sách sản phẩm "Tòa nhà nguyên căn" diện tích >1000
              <a
                href={`/${
                  pathname.split("/")[1]
                }/search?type=${type}&min_area=${1000}`}
              >
                <div className="item-img">
                  <Image src={mbkd3} alt="Tòa nhà nguyên căn" />
                </div>
                <div className="item-text">{text3}</div>
              </a>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
