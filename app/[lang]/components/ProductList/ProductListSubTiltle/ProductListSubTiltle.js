import React from "react";
import "../../../../../css/ProductListSubTiltle.css";

export default function ProductListSubTiltle({ title, text }) {
  return (
    <div className="ProductListSubTiltle pb-16 text-center space-y-6">
      <h1 className="title--section--1 font-semibold px-[5.5%] md:px-0  mx-auto">
        {title}
      </h1>
      <div className="px-[5.5%] md:px-[23%]">
        <p className="roboto-font">{text}</p>
      </div>
    </div>
  );
}
