import "../../../../../css/ProductListLienHe.css";
import React from "react";
import FormLienHe from "../../FormLienHe/FormLienHe";

export default function ProductListLienHe({ question, dict }) {
  return (
    <div className="ProductListLienHe  pt-[54px] lg:pt-16 pb-[60px]  lg:pb-[120px]">
      <div className="container-2 text-center roboto-font space-y-3 md:space-y-4 ">
        <div className="question">
          <p>{question}</p>
        </div>
        <div className="hotline">
          <a
            href={`tel:${dict?.setting?.hotline}`}
            className="space-y-2 lg:space-y-0"
          >
            <span className="block lg:inline">
              {dict?.general?.pls_contact_us}
            </span>{" "}
            <span className="text-orange block lg:inline">
              {dict?.setting?.hotline}
            </span>
          </a>
        </div>
        <div className="lien-he">
          <p className="space-y-2 lg:space-y-0">
            <span className="block lg:inline">{dict?.general?.or}</span>{" "}
            <span className="block lg:inline">
              <FormLienHe text={dict?.general?.leave_information} dict={dict} />
            </span>{" "}
            <span className="block lg:inline">
              {dict?.general?.to_receive_a_timely_quote_for_the_latest_space}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}
