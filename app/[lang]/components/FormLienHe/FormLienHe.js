"use client";
import React from "react";
import "../../../../css/FormLienHe.css";
import { Button, Modal, message } from "antd";
import { useState } from "react";

export default function FormLienHe({ text, dict }) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // regexPhoneNumber
  let regexPhoneNumber = (phone) => {
    const regex = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
    var isValid = regex.test(phone);
    if (!isValid) {
      document.getElementById("phone_warning").classList.remove("hidden");
      return false;
    } else {
      document.getElementById("phone_warning").classList.add("hidden");
      return true;
    }
  };

  let regexEmail = (email) => {
    const regex =
      /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    var isValid = regex.test(email);
    if (!isValid) {
      document.getElementById("email_warning").classList.remove("hidden");
      return false;
    } else {
      document.getElementById("email_warning").classList.add("hidden");
      return true;
    }
  };

  let checkForm = (event) => {
    event.preventDefault();
    let check = true;
    const data = new FormData(event.target);
    const inputObject = Object.fromEntries(data);
    check = regexPhoneNumber(inputObject.phone) & regexEmail(inputObject.email);
    if (check) {
      message.success(dict?.general?.submit_a_successful_contact_request);
      setIsModalOpen(false);
    }
  };
  return (
    <div className="FormLienHeModal roboto-font">
      {/* Button Lien He */}
      <button onClick={showModal} className="main-button px-[30px] roboto-font">
        {text}
      </button>
      {/* Form Lien He */}
      <Modal
        title={dict?.general?.contact_request}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        bodyStyle={{ width: "100%" }}
      >
        <form onSubmit={checkForm}>
          <div className=" py-6 px-5 md:px-10 space-y-6">
            {/* Họ tên */}
            <div className="mb-4">
              <label className="block mb-2" htmlFor="username">
                {dict?.general?.name} <span className="iconImportant">*</span>
              </label>
              <input
                className="border rounded w-full  text-gray-700 leading-tight p-[20px]"
                id="username"
                name="username"
                type="text"
                placeholder="Nguyễn Văn A..."
                required
              />
            </div>
            {/* Email */}
            <div className="mb-4">
              <label className="block mb-2" htmlFor="email">
                {dict?.general?.email_work}{" "}
                <span className="iconImportant">*</span>
              </label>
              <input
                className="border rounded w-full  text-gray-700 leading-tight p-[20px]"
                id="email"
                name="email"
                type="text"
                placeholder="...@company"
                required
              />
              <p
                className="mt-1 text-red-500 text-sm hidden"
                id="email_warning"
              >
                Email không hợp lệ.
              </p>
            </div>
            {/* Điện thoại */}
            <div className="mb-4">
              <label className="block mb-2" htmlFor="phone">
                {dict?.general?.phone} <span className="iconImportant">*</span>
              </label>
              <input
                className="border rounded w-full  text-gray-700 leading-tight p-[20px]"
                id="phone"
                name="phone"
                type="text"
                placeholder="0912345678"
                required
              />
              <p
                className="mt-1 text-red-500 text-sm hidden"
                id="phone_warning"
              >
                Số điện thoại không hợp lệ.
              </p>
            </div>
            {/* Tên công ty */}
            <div className="mb-4">
              <label className="block mb-2" htmlFor="company">
                {dict?.general?.company}
              </label>
              <input
                className="border rounded w-full  text-gray-700 leading-tight p-[20px]"
                id="company"
                name="company"
                type="text"
                placeholder="Công ty TNHH A..."
              />
            </div>
            {/* Chi tiết nhu cầu */}
            <div className="mb-4">
              <label className="block mb-2">
                {dict?.general?.need_details}
              </label>
              <textarea
                className="w-full border rounded  text-gray-700  leading-tight p-[20px]"
                id="demand"
                name="demand"
                defaultValue={"Diện tích, khu vực, giá thuê..."}
              ></textarea>
            </div>
          </div>
          {/* Modal footer */}
          <div className="flex items-center justify-center p-6 space-x-2 rounded-b">
            <button className="main-button uppercase px-[83px]" type="submit">
              {dict?.general?.button_open_contact_form}
            </button>
          </div>
        </form>
      </Modal>
    </div>
  );
}
