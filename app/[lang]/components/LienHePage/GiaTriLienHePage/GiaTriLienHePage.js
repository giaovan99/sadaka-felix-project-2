import React from "react";
import "../../../../../css/GiaTriLienHePage.css";
import Image from "next/image";
import VanPhongImg from "../../../../../public/img/img-lien-he/Rectangle 116.jpg";

export default function GiaTriLienHePage({ dict }) {
  return (
    <div className="GiaTriLienHePage pt-16 md:pt-24 bg-white">
      <div className=" flex flex-col md:grid md:grid-cols-2 container-2 ">
        <div className="left-side">
          <Image
            src={VanPhongImg}
            alt="Hình ảnh mẫu"
            className="w-full object-cover object-center h-full aspect-square md:aspect-auto"
          />
        </div>
        <div className="right-side flex flex-col justify-center bg-white py-6 px-3 md:py-[8%] md:px-[10%] lg:py-[10%] lg:px-[15%]">
          <div className="title title--section--2 font-medium md:font-bold mb-6 md:mb-12">
            <h3>{dict?.contact?.our_value?.name}</h3>
          </div>

          <div
            dangerouslySetInnerHTML={{
              __html: dict?.contact?.our_value?.content,
            }}
          />
        </div>
      </div>
    </div>
  );
}
