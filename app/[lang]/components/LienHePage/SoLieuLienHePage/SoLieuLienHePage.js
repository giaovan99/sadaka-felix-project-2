import React from "react";
import "../../../../../css/SoLieuLienHePage.css";
import Image from "next/image";
import Item1 from "../../../../../public/img/img-lien-he/Rectangle 115.jpg";
import Item2 from "../../../../../public/img/img-lien-he/Rectangle 115 (1).jpg";
import Item3 from "../../../../../public/img/img-lien-he/Rectangle 115 (2).jpg";
export default function SoLieuLienHePage({ dict }) {
  return (
    <div className="SoLieuLienHePage pt-8 md:pt-24 bg-white">
      <div className="container-1">
        <div className="grid grid-cols-1 space-y-6 md:space-y-0 md:grid-cols-3 md:gap-7 lg:gap-20 w-full">
          <div className="item space-y-4 md:space-y-7">
            <Image src={Item1} alt="Hình ảnh" />
            <div className="disc space-y-1.5 ">
              <div className="num roboto-font">3000+</div>
              <div className="text uppercase">{dict?.contact?.product}</div>
            </div>
          </div>
          <div className="item space-y-4 md:space-y-7">
            <Image src={Item2} alt="Hình ảnh" />
            <div className="disc space-y-1.5 ">
              <div className="num roboto-font">5000+</div>
              <div className="text uppercase">{dict?.contact?.customers}</div>
            </div>
          </div>
          <div className="item space-y-4 md:space-y-7">
            <Image src={Item3} alt="Hình ảnh" />
            <div className="disc space-y-1.5 ">
              <div className="num roboto-font">50+</div>
              <div className="text uppercase">{dict?.contact?.consultants}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
