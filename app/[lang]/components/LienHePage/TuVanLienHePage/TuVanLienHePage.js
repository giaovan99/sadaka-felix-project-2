import React from "react";
import "../../../../../css/TuVanLienHePage.css";
import Image from "next/image";
import VanPhongImg from "../../../../../public/img/img-lien-he/Rectangle 118.jpg";
import FormLienHe from "../../FormLienHe/FormLienHe";

export default function TuVanLienHePage({ dict }) {
  return (
    <div className="TuVanLienHePage md:bg-white pt-16 pb-16 md:pb-0 md:pt-0">
      <div className=" flex flex-col-reverse md:grid md:grid-cols-2 container-2">
        <div className="right-side  flex flex-col justify-center bg-white py-6 px-3 md:py-[8%] md:px-[10%] lg:py-[10%] lg:px-[15%]">
          <div className="title title--section--2 font-medium md:font-bold mb-5">
            <h3>
              {
                dict?.contact?.help_you_quickly_find_the_best_office_and_space
                  ?.name
              }
            </h3>
          </div>
          <div
            dangerouslySetInnerHTML={{
              __html:
                dict?.contact?.help_you_quickly_find_the_best_office_and_space
                  ?.content,
            }}
          />
          <div className="flex justify-center md:justify-start">
            <FormLienHe
              text={dict?.general?.button_open_contact_form}
              dict={dict}
            />
          </div>
        </div>
        <div className="left-side">
          <Image
            src={VanPhongImg}
            alt="Hình ảnh mẫu"
            className="w-full object-cover object-center h-full aspect-square md:aspect-auto"
          />
        </div>
      </div>
    </div>
  );
}
