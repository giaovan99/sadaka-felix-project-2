"use client";
import React from "react";
import "../../../../../css/BannerLienHePage.css";
import Image from "next/image";
import Banner from "../../../../../public/img/img-lien-he/Rectangle 98.jpg";
import { HiOutlineLocationMarker } from "react-icons/hi";
import { MdOutlineMarkEmailUnread } from "react-icons/md";

import { FiPhoneCall } from "react-icons/fi";
import Link from "next/link";

export default function BannerLienHePage({ dict }) {
  // console.log(dict?.contact?.description);
  return (
    <div className="BannerLienHePage mb-10 md:mb-[120px]">
      <div className="title container-2">
        <h3 className="title--section--5 text-orange mt-10 mb-6 md:my-10 text-center md:text-left">
          {dict?.general?.contact}
        </h3>
      </div>
      <div className="banner container-2">
        <Image src={Banner} alt="Banner" />
      </div>
      <div className="contact-info container-1  ">
        <div className="items h-full w-full bg-white flex items-center px-[5%]">
          <div className="grid grid-cols-1 md:grid-cols-3 w-full md:gap-2 space-y-9 md:space-y-0">
            <div className=" hotline text-center">
              <a
                href={`tel:${dict?.setting?.hotline}`}
                className="flex items-center space-x-3 md:space-x-0 md:block"
              >
                <div className="icon flex justify-center md:mb-2">
                  <FiPhoneCall />
                </div>
                <div className="space-y-1 md:space-y-0">
                  <div className="title">
                    <p className="title--section--4 ">{dict?.general?.phone}</p>
                  </div>
                  <div className="text roboto-font">
                    <p className="text-left md:text-center roboto-font">
                      {dict?.setting?.hotline}
                    </p>
                  </div>
                </div>
              </a>
            </div>
            <div className="email text-center">
              <Link
                href={`mailto:${dict?.setting?.email}`}
                className="flex items-center space-x-3 md:space-x-0 md:block"
              >
                <div className="icon flex justify-center md:mb-2">
                  <MdOutlineMarkEmailUnread />
                </div>
                <div className="space-y-1 md:space-y-0">
                  <div className="title">
                    <p className="title--section--4 text-left md:text-center ">
                      Email
                    </p>
                  </div>
                  <div className="text roboto-font">
                    <p className="text-left md:text-center roboto-font">
                      {dict?.setting?.email}
                    </p>
                  </div>
                </div>
              </Link>
            </div>
            <div className="flex items-center md:block  address text-center space-x-3 md:space-x-0">
              <div className="icon flex justify-center mb-2">
                <HiOutlineLocationMarker />
              </div>
              <div className="">
                <div className="title">
                  <p className="title--section--4 text-left md:text-center">
                    {dict?.general?.address}
                  </p>
                </div>
                <div className="text roboto-font">
                  <p className="text-left md:text-center roboto-font">
                    {dict?.setting?.address}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="disc container-1 text-center mt-[-30px] lg:mt-[-90px] ">
        <div className=" px-0 xl:px-[7.5%] space-y-6">
          <div className="title title--section--2 ">
            <h3
              className="raleway-font"
              dangerouslySetInnerHTML={{
                __html: dict?.contact?.about_us?.description,
              }}
            />
          </div>
          <div
            dangerouslySetInnerHTML={{
              __html: dict?.contact?.about_us?.content,
            }}
          />
        </div>
      </div>
    </div>
  );
}
