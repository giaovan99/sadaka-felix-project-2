"use client";
import React, { useEffect, useState } from "react";
import "../../../../css/footer.css";
import Logo from "../../../../public/img/img-footer/Logo_FOffice_v3_nen_xanh 1.png";
import Logo2 from "../../../../public/img/img-footer/Group 278.jpg";
import Image from "next/image";
import { GoLocation } from "react-icons/go";
import { MdOutlineMarkEmailUnread } from "react-icons/md";
import { FiPhoneCall } from "react-icons/fi";
import {
  SlSocialYoutube,
  SlSocialLinkedin,
  SlSocialFacebook,
} from "react-icons/sl";
import Link from "next/link";
import { getDictionary } from "../../distionaries";

export default function Footer({ lang }) {
  const [footerData, setFooterData] = useState({});
  const { general, type, setting } = footerData;
  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    // console.log(res);
    if (res) {
      setFooterData(res);
    }
  };
  useEffect(() => {
    getDict(lang);
  }, [lang]);

  return (
    <footer>
      <div className="container-2">
        {/* Logo */}
        <div className="footer__logo w-full flex justify-center">
          <Image src={Logo} alt="Logo Felix Office" />
        </div>
        {/* Các phần khác */}
        <div className="footer__content w-full flex flex-col lg:flex-row  text-white pt-6 lg:pt-[32px] lg:space-x-[40px] 2xl:space-x-[90px]">
          {/* Liên hệ */}
          <div className="content content-1 ">
            <div className="content__title">
              <h3>{general?.contact}: </h3>
            </div>
            <div className="content__disc space-y-[16px] roboto-font">
              <div className="flex space-x-[12px] lg:space-x-[10px] items-center">
                <p className="disc__icon under-img">
                  <GoLocation />
                </p>
                <p className="disc__text">
                  <b>{general?.address}: </b> <br />
                  <span>{setting?.address}</span>
                </p>
              </div>
              <div className="flex space-x-[12px] lg:space-x-[10px] items-center">
                <p className="disc__icon ">
                  <MdOutlineMarkEmailUnread className="outline-none" />
                </p>
                <p className="disc__text">
                  <b>{general?.email_work}: </b>
                  <a href={`mailto:${setting?.email}`}>{setting?.email}</a>
                </p>
              </div>
              <div className="flex space-x-[12px] lg:space-x-[10px] items-center">
                <p className="disc__icon stroke-27">
                  <FiPhoneCall />
                </p>
                <p className="disc__text">
                  <b>{general?.phone}: </b>
                  <a href={`tel:${setting?.hotline}`}>{setting?.hotline}</a>
                </p>
              </div>
            </div>
          </div>
          {/* Dịch vụ, tiện ích, social media */}
          <div className="flex grow pb-14 md:pb-0 md:justify-between pt-6 md:pt-10 lg:pt-0 lg:space-x-[40px] 2xl:space-x-[90px]">
            {/* Dịch vụ */}
            <div className="content content-2 w-1/2 md:w-fit">
              <div className="content__title">
                <h3>{general?.service}</h3>
              </div>
              <div className="content__disc leading-7 space-y-2 roboto-font">
                <p className="disc__text">
                  <Link href={`/${lang}/san-van-phong`}>
                    {type?.officeFloor}
                  </Link>
                </p>
                <p className="disc__text">
                  <Link
                    href={`/${lang}/van-phong-tron-goi`}
                    className="disc__text"
                  >
                    {type?.allInclusiveOffice}
                  </Link>
                </p>
                <p className="disc__text">
                  <Link href={`/${lang}/toa-nha`} className="disc__text">
                    {type?.officeBuilding}
                  </Link>
                </p>
                <p className="disc__text">
                  <Link
                    href={`/${lang}/mat-bang-kinh-doanh`}
                    className="disc__text"
                  >
                    {type?.businessPremises}
                  </Link>
                </p>
                <p className="disc__text">
                  <Link href={`/${lang}/ban`} className="disc__text">
                    {general?.sell}
                  </Link>
                </p>
              </div>
            </div>
            {/* Tiện ích */}
            <div className="content content-3 w-1/2 md:w-fit">
              <div className="content__title">
                <h3>{general?.utilities}</h3>
              </div>
              <div className="content__disc leading-7 space-y-2 roboto-font">
                <p className="disc__text">
                  <Link href={`/${lang}/lien-he`}>
                    {general?.cooperation_consignment_products}
                  </Link>
                </p>
                <p className="disc__text">
                  <Link href="#" className="disc__text">
                    {general?.new}
                  </Link>
                </p>
              </div>
            </div>
            {/* Social Media */}
            <div className="hidden content content-4 md:flex space-x-[52px] ">
              <div className="content__img min-w-[70px]">
                <Image src={Logo2} alt="Logo" width={108} height={108} />
              </div>
              <div className="content__social flex space-x-6">
                <div className="social_icon">
                  <a href={`${setting?.social_youtube}`} className="ytbIcon">
                    <SlSocialYoutube />
                  </a>
                </div>
                <div className="social_icon linkedInIcon">
                  <a href={`${setting?.social_facebook}`}>
                    <SlSocialFacebook />
                  </a>
                </div>
                <div className="social_icon linkedInIcon">
                  <a href={`${setting?.social_instagram}`}>
                    <SlSocialLinkedin />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Socail Mobile */}
      <div className="content content-5 md:hidden pt-6 pb-10  bg-white ">
        <div className="content__social flex space-x-8 justify-center container-1">
          <div className="social_icon fbIcon">
            <a href={`${setting?.social_facebook}`} target="_blank">
              <SlSocialFacebook />
            </a>
          </div>
          <div className="social_icon">
            <a
              href={`${setting?.social_youtube}`}
              target="_blank"
              className="ytbIcon"
            >
              <SlSocialYoutube />
            </a>
          </div>
          <div className="social_icon linkedInIcon">
            <a href={`${setting?.social_instagram}`} target="_blank">
              <SlSocialLinkedin />
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}
