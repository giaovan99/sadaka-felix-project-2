import { Spin } from "antd";
import React from "react";

export default function Spinner() {
  return (
    <div>
      <Spin />
    </div>
  );
}
