import React from "react";
import "../../../../../css/ServiceHomePage.css";
import Image from "next/image";
import Icon1 from "../../../../../public/img/img-home/mdi_floor-plan.png";
import Icon2 from "../../../../../public/img/img-home/carbon_building-insights-2.png";
import Icon3 from "../../../../../public/img/img-home/bi_building-check.png";
import Icon4 from "../../../../../public/img/img-home/la_store-alt.png";
import Link from "next/link";

export default function Service({ dict }) {
  const { home, general } = dict;
  const typeEstate = dict.type;
  // console.log(dict);
  return (
    <section className="ServiceHomePage space-y-12 lg:space-y-14 mb-[60px] lg:mb-[120px]">
      <div className="container-1">
        <div className="title--section--2 text-center">
          <h3 className="mb-5 ">
            {home?.we_provide_4_types_of_services_including}
          </h3>
          <p className="roboto-font">
            {home?.office_floor_for_rent_in_the_building_whole_building}
          </p>
        </div>
      </div>
      <div className="container-2 items">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 space-y-12 md:space-y-0">
          {/* Sàn văn phòng */}
          <div className="item flex flex-col justify-center items-center space-y-1 lg:space-y-3 lg:px-2">
            <div className="item-icon">
              <Image src={Icon1} width={100} height={100} alt="..." />
            </div>
            <div className="item-info flex-1 space-y-1.5 lg:space-y-2">
              <p className="item-title capitalize ">
                {typeEstate?.officeFloor}
              </p>
              <div
                className="roboto-font"
                dangerouslySetInnerHTML={{
                  __html:
                    general?.office_floor_full_and_detailed_information_about,
                }}
              />
            </div>
            <div className="item-chi-tiet h-max	roboto-font">
              <Link
                href="/san-van-phong"
                className="main-button px-10 inline-block "
              >
                {general?.detail} &nbsp; &rsaquo; &rsaquo;
              </Link>
            </div>
          </div>
          {/* Toà nhà nguyên căn */}
          <div className="item flex flex-col justify-center items-center space-y-1 lg:space-y-3 lg:px-2">
            <div className="item-icon">
              <Image src={Icon2} width={100} height={100} alt="..." />
            </div>
            <div className="item-info flex-1 space-y-1.5 lg:space-y-2">
              <p className="item-title capitalize ">
                {typeEstate?.officeBuilding}
              </p>
              <div
                className="roboto-font"
                dangerouslySetInnerHTML={{
                  __html:
                    general?.original_building_suitable_to_rent_as_company,
                }}
              />
            </div>
            <div className="item-chi-tiet h-max roboto-font	">
              <Link href="/toa-nha" className="main-button px-10 inline-block ">
                {general?.detail} &nbsp; &rsaquo; &rsaquo;
              </Link>
            </div>
          </div>
          {/* Văn phòng trọn gói */}
          <div className="item flex flex-col justify-center items-center space-y-1 lg:space-y-3 lg:px-2">
            <div className="item-icon">
              <Image src={Icon3} width={100} height={100} alt="..." />
            </div>
            <div className="item-info flex-1 space-y-1.5 lg:space-y-2">
              <p className="item-title capitalize ">
                {typeEstate?.allInclusiveOffice}
              </p>
              <div
                className="roboto-font"
                dangerouslySetInnerHTML={{
                  __html: general?.complete_office_information_about_the_units,
                }}
              />
            </div>
            <div className="item-chi-tiet h-max	roboto-font">
              <Link
                href="/van-phong-tron-goi"
                className="main-button px-10 inline-block "
              >
                {general?.detail} &nbsp; &rsaquo; &rsaquo;
              </Link>
            </div>
          </div>
          {/* Mặt bằng kinh doanh */}
          <div className="item flex flex-col justify-center items-center space-y-1 lg:space-y-3 lg:px-2">
            <div className="item-icon">
              <Image src={Icon4} width={100} height={100} alt="..." />
            </div>
            <div className="item-info flex-1 space-y-1.5 lg:space-y-2">
              <p className="item-title capitalize ">
                {general?.business_premises}
              </p>
              <div
                className="roboto-font"
                dangerouslySetInnerHTML={{
                  __html: general?.business_premises_suitable_to_rent_for_banks,
                }}
              />
            </div>
            <div className="item-chi-tiet h-max roboto-font	">
              <Link
                href="/mat-bang-kinh-doanh"
                className="main-button px-10 inline-block "
              >
                {general?.detail} &nbsp; &rsaquo; &rsaquo;
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
