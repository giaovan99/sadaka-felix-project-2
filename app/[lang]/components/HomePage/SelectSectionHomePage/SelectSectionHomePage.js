"use client";
import { Select, Space } from "antd";
import React, { useState } from "react";
import "../../../../../css/SelectSectionHomePage.css";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function SelectSectionHomePage({ dict }) {
  const { home, general } = dict;
  const pathname = usePathname();
  const typeEstate = dict.type;
  const [selectOption, setSelectOption] = useState(`/san-van-phong`);
  const handleChange = (value) => {
    if (value == 1) {
      setSelectOption(`/${pathname.split("/")[1]}/san-van-phong`);
    } else if (value == 2) {
      setSelectOption(`/${pathname.split("/")[1]}/toa-nha`);
    } else if (value == 3) {
      setSelectOption(`/${pathname.split("/")[1]}/van-phong-tron-goi`);
    } else if (value == 4) {
      setSelectOption(`/${pathname.split("/")[1]}/mat-bang-kinh-doanh`);
    }
  };
  return (
    <div className="SelectSectionHomePage pt-24 mb-[120px] ">
      <div className="title text-center text-white mb-6">
        <h1 className="title--section--1 font-normal">{home?.top_quality}</h1>
        <h1 className="title--section--1 font-bold">
          {home?.absolute_satisfaction}
        </h1>
      </div>
      <div className="disc mb-8 text-center container-1 ">
        <p className="text-white">
          {home?.felix_office_provides_leading_professional_office}
        </p>
      </div>
      <div className="select-option flex justify-center roboto-font ">
        <Space>
          <Select
            defaultValue="1"
            style={{
              width: 305,
              height: 54,
            }}
            onChange={handleChange}
            options={[
              {
                value: "1",
                label: typeEstate?.officeFloor,
              },
              {
                value: "2",
                label: typeEstate?.officeBuilding,
              },
              {
                value: "3",
                label: typeEstate?.allInclusiveOffice,
              },
              {
                value: "4",
                label: typeEstate?.businessPremises,
              },
            ]}
          />
        </Space>
      </div>
      <div className="select-button-confirm flex justify-center mt-[12px] roboto-font">
        <Link
          href={selectOption}
          className="w-[305px] flex justify-center main-button"
        >
          {general?.select_service}
        </Link>
      </div>
    </div>
  );
}
