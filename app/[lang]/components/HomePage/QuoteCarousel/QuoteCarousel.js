"use client";
import React from "react";
import { Carousel } from "antd";
import "../../../../../css/QuoteCarousel.css";

export default function Quotes({ dict }) {
  const { testimonials } = dict;
  const renderQuotes = () => {
    return testimonials.map((item, index) => (
      <div className="title--section--2  text-center font-thin " key={index}>
        <h3 className="text-orange">
          <span>“ </span>
          {item.content} <span>”</span>
        </h3>
        <p className="quote-text font-medium text-center pt-5 lg:pt-9 pb-5 lg:pb-14">
          {item.name} - {item.company}
        </p>
      </div>
    ));
  };
  return (
    <>
      {testimonials && (
        <div className="QuotesCarousel container-1 ">
          {testimonials.length > 0 && (
            <div className="mb-[60px] lg:mb-[120px]">
              <Carousel autoplay autoplaySpeed={10000} infinite>
                {renderQuotes()}
              </Carousel>
            </div>
          )}
        </div>
      )}
    </>
  );
}
