"use client";
import React from "react";
import { Carousel } from "antd";
import Image from "next/image";
import "../../../../../css/BannerHomePage.css";
import { BsArrowRightShort } from "react-icons/bs";

import Banner1 from "../../../../../public/img/img-home/Rectangle 88.jpg";

const carouselRef = React.createRef();
const settings = {
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  swipeToSlide: true,
};

const settings2 = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  autoplay: true,
  swipeToSlide: true,
};

export default function BannerHomePage({ dict }) {
  // console.log("bản ngôn ngữ được chọn: ", dict);
  const renderSliderDesk = () => {
    return dict?.slider?.map((item, index) => (
      <div className="item" key={index}>
        <div className="item-img">
          <img src={item?.image} alt="..." />
          <div className="item-footer">
            <p className="text-orange">{index + 1}</p>
            <p>{dict?.slider.length}</p>
            <button
              onClick={() => {
                carouselRef.current.next();
              }}
              className="pl-3 font-bold"
            >
              <BsArrowRightShort />
            </button>
          </div>
        </div>
        <div className="item-info flex space-x-3">
          <div className="item-info-icon text-orange">/</div>
          <div className="item-info-text space-y-2.5">
            <p className="title--section--4">{item?.name}</p>
            {/* <p className="roboto-font">{item?.description}</p> */}
            <div
              className="roboto-font"
              dangerouslySetInnerHTML={{
                __html: item?.description,
              }}
            />
          </div>
        </div>
      </div>
    ));
  };
  const renderSliderMobile = () => {
    return dict?.slider?.map((item, index) => (
      <div className="item px-3.5" key={index}>
        <div className="item-img h-[388px] mb-6 ">
          <img
            src={item?.image}
            alt="..."
            className="w-full object-cover object-center h-full"
          />
        </div>
        <div className="item-info flex space-x-3">
          <div className="item-info-icon text-orange">/</div>
          <div className="item-info-text space-y-2.5">
            <p className="title--section--4">{item?.name}</p>
            <p className="roboto-font">{item?.description}</p>
          </div>
        </div>
      </div>
    ));
  };
  return (
    <div className="bannerHomePage mb-[60px] lg:mb-[120px] ">
      {/* Carousel for desktop */}
      <div className="container-1 hidden lg:block">
        <Carousel effect="fade" ref={carouselRef} {...settings}>
          {dict?.slider && renderSliderDesk()}
        </Carousel>
      </div>
      {/* Carousel for mobile */}
      <div className="lg:hidden">
        <Carousel {...settings2} centerMode>
          {/* item 1 */}
          <div className="item px-3.5">
            <div className="item-img h-[388px] mb-6 ">
              <Image
                src={Banner1}
                alt="..."
                className="w-full object-cover object-center h-full"
              />
            </div>
            <div className="item-info flex space-x-3">
              <div className="item-info-icon text-orange">/</div>
              <div className="item-info-text space-y-2.5">
                <p className="title--section--4">UPGen Deutsches Haus</p>
                <p className="roboto-font">
                  33 Đ. Lê Duẩn, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh
                </p>
              </div>
            </div>
          </div>

          {/* item 2 */}
          <div className="item px-3.5">
            <div className="item-img h-[388px] mb-6 ">
              <Image
                src={Banner1}
                alt="..."
                className="w-full object-cover object-center h-full"
              />
            </div>
            <div className="item-info flex space-x-3">
              <div className="item-info-icon text-orange">/</div>
              <div className="item-info-text space-y-2.5">
                <p className="title--section--4">UPGen Deutsches Haus</p>
                <p className="roboto-font">
                  33 Đ. Lê Duẩn, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh
                </p>
              </div>
            </div>
          </div>

          {/* item 3 */}
          <div className="item px-3.5">
            <div className="item-img h-[388px] mb-6 ">
              <Image
                src={Banner1}
                alt="..."
                className="w-full object-cover object-center h-full"
              />
            </div>
            <div className="item-info flex space-x-3">
              <div className="item-info-icon text-orange">/</div>
              <div className="item-info-text space-y-2.5">
                <p className="title--section--4">UPGen Deutsches Haus</p>
                <p className="roboto-font">
                  33 Đ. Lê Duẩn, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh
                </p>
              </div>
            </div>
          </div>

          {/* item 4 */}
          <div className="item px-3.5">
            <div className="item-img h-[388px] mb-6 ">
              <Image
                src={Banner1}
                alt="..."
                className="w-full object-cover object-center h-full"
              />
            </div>
            <div className="item-info flex space-x-3">
              <div className="item-info-icon text-orange">/</div>
              <div className="item-info-text space-y-2.5">
                <p className="title--section--4">UPGen Deutsches Haus</p>
                <p className="roboto-font">
                  33 Đ. Lê Duẩn, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh
                </p>
              </div>
            </div>
          </div>

          {/* item 5 */}
          <div className="item px-3.5">
            <div className="item-img h-[388px] mb-6 ">
              <Image
                src={Banner1}
                alt="..."
                className="w-full object-cover object-center h-full"
              />
            </div>
            <div className="item-info flex space-x-3">
              <div className="item-info-icon text-orange">/</div>
              <div className="item-info-text space-y-2.5">
                <p className="title--section--4">UPGen Deutsches Haus</p>
                <p className="roboto-font">
                  33 Đ. Lê Duẩn, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh
                </p>
              </div>
            </div>
          </div>

          {/* item 6 */}
          <div className="item px-3.5">
            <div className="item-img h-[388px] mb-6 ">
              <Image
                src={Banner1}
                alt="..."
                className="w-full object-cover object-center h-full"
              />
            </div>
            <div className="item-info flex space-x-3">
              <div className="item-info-icon text-orange">/</div>
              <div className="item-info-text space-y-2.5">
                <p className="title--section--4">UPGen Deutsches Haus</p>
                <p className="roboto-font">
                  33 Đ. Lê Duẩn, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh
                </p>
              </div>
            </div>
          </div>
          {dict?.slider && renderSliderMobile()}
        </Carousel>
      </div>
    </div>
  );
}
