import React from "react";
import "../../../../../css/DinhNghiaVanPhongTronGoi.css";

export default function DinhNghiaVanPhongTronGoi({ dict }) {
  return (
    <div className="DinhNghiaVanPhongTronGoi container-1 space-y-6">
      <div className="title text-center">
        <h1 className="title--section--1 font-semibold">
          {dict?.package_office?.what_is_a_full_service_office}
        </h1>
      </div>
      <div
        className="content roboto-font"
        dangerouslySetInnerHTML={{
          __html:
            dict?.package_office?.all_inclusive_office_is_a_type_of_office_that,
        }}
      ></div>
    </div>
  );
}
