"use client";
import React, { Component, useEffect, useState } from "react";
import "../../../../../css/DoiTacVanPhongTronGoi.css";
import { Carousel } from "antd";
import { HiArrowLeft, HiArrowRight } from "react-icons/hi";
import { vptgService } from "@/service/vptg";
const carouselRef = React.createRef();

export default function DoiTacVanPhongTronGoi({ dict }) {
  const [items, setItems] = useState([]);
  let getCorp = async () => {
    console.log("lala");
    let res = await vptgService.getCorp();
    if (res.data.success) {
      setItems(res.data.data);
    }
  };
  useEffect(() => {
    // Gọi API lấy danh sách đối tác
    getCorp();
  }, []);
  const settings = {
    // className: "center",
    // centerMode: true,
    // infinite: true,
    // centerPadding: "0px",
    // slidesToShow: 5,
    // speed: 500,
    // rows: 2,
    // slidesPerRow: 1,
    // dots: false,
    // className: "center",
    // centerMode: true,
    infinite: true,
    // centerPadding: "10px",
    // gap: "10px",
    slidesToShow: 5,
    speed: 500,
    rows: 2,
    slidesPerRow: 1,
    dots: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          rows: 1,
          dots: false,
        },
      },
    ],
  };

  return (
    <div className="DoiTacVanPhongTronGoi container-2 mt-16 md:mt-[120px] space-y-5 md:space-y-11">
      <div className="title title--section--2 text-center">
        <h3 className="font-bold">
          {dict?.package_office?.trusted_service_providers}
        </h3>
      </div>
      <div className="carouselPartner flex items-center">
        <div className="w-[10%] hidden md:block">
          <button
            onClick={() => {
              carouselRef.current.prev();
            }}
          >
            <HiArrowLeft />
          </button>
        </div>
        <div className=" w-full md:w-[80%]">
          <Carousel {...settings} ref={carouselRef} autoplay>
            {items.length > 0 &&
              items.map((item) => (
                <div className="item" key={item.name}>
                  <img src={item.image} alt={item.name} />
                </div>
              ))}
          </Carousel>
        </div>
        <div className="w-[10%] hidden md:flex md:justify-end ">
          <button
            onClick={() => {
              carouselRef.current.next();
            }}
          >
            <HiArrowRight />
          </button>
        </div>
      </div>
    </div>
  );
}
