import React from "react";
import "../../../../../css/LoiIchVanPhongTronGoi.css";
import Image from "next/image";
import VanPhongImg from "../../../../../public/img/img-van-phong-tron-goi/Rectangle 118.jpg";

export default function LoiIchVanPhongTronGoi({ dict }) {
  const { package_office } = dict;

  return (
    <div className="LoiIchVanPhongTronGoi flex flex-col-reverse md:grid md:grid-cols-2 container-2 mt-12 md:mt-0">
      <div className="right-side  flex flex-col justify-center bg-white px-3 pt-4 pb-6 md:py-[10%] md:px-[15%]">
        <div className="title title--section--2 font-bold mb-6 md:mb-12">
          <h3>{package_office?.benefits_of_all_inclusive_office?.name}</h3>
        </div>
        <div
          dangerouslySetInnerHTML={{
            __html: package_office?.benefits_of_all_inclusive_office?.content,
          }}
        />
      </div>
      <div className="left-side">
        <Image
          src={VanPhongImg}
          alt="Hình ảnh mẫu"
          className="w-full object-cover object-center h-full aspect-square md:aspect-auto"
        />
      </div>
    </div>
  );
}
