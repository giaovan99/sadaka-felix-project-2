"use client";
import React from "react";
import "../../../../../css/BannerVanPhongTronGoi.css";
import Image from "next/image";
import Banner from "../../../../../public/img/img-van-phong-tron-goi/Rectangle 59.jpg";

export default function BannerVanPhongTronGoi({ dict }) {
  return (
    <div className="BannerVanPhongTronGoi container-2 space-y-10 mt-12 lg:mt-16">
      <div className="title">
        <p className="title--section--5 text-orange text-center md:text-left">
          {dict?.office_floor?.felix_office_provides_all_inclusive_office}
        </p>
      </div>
      <div className="banner">
        <Image src={Banner} alt="..." />
        <div className="banner-content">
          <p className="banner-text roboto-font">
            {dict?.office_floor?.currently_felix_office_is_a_partner_with}
          </p>
        </div>
      </div>
    </div>
  );
}
