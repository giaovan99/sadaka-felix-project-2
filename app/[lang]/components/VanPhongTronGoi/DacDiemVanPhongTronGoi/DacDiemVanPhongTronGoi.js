import React from "react";
import "../../../../../css/DacDiemVanPhongTronGoi.css";
import Image from "next/image";
import VanPhongImg from "../../../../../public/img/img-van-phong-tron-goi/Rectangle 116.jpg";

export default function DacDiemVanPhongTronGoi({ dict }) {
  const { package_office } = dict;
  return (
    <div className="DacDiemVanPhongTronGoi flex flex-col md:grid md:grid-cols-2 container-2 mt-12 lg:mt-[120px] ">
      <div className="left-side">
        <Image
          src={VanPhongImg}
          alt="Hình ảnh mẫu"
          className="w-full object-cover object-center h-full "
        />
      </div>
      <div className="right-side  flex flex-col justify-center bg-white px-3 pt-4 pb-6 md:py-[10%] md:px-[15%]">
        <div className="title title--section--2 font-bold mb-6 md:mb-12">
          <h3>{package_office?.features_of_all_inclusive_offices?.name}</h3>
        </div>
        {/* <div className="content space-y-8">
          <div className="item space-y-2 md:space-y-4">
            <p className="title--section--4 font-bold">Phòng riêng</p>
            <p className="roboto-font">
              Các văn phòng riêng được setup sẵn bàn ghế nội thất với các diện
              tích linh hoạt cho 4-8-12-16...người làm việc
            </p>
          </div>
          <div className="item space-y-2 md:space-y-4">
            <p className="title--section--4 font-bold">Không gian chung</p>
            <p className="roboto-font">
              Khu vực chung được thiết kế hiện đại, thân thiện mang đến tính
              giao lưu, kết nối, năng động và khả năng sáng tạo trong công việc.
            </p>
          </div>
          <div className="item space-y-2 md:space-y-4">
            <p className="title--section--4 font-bold">Văn phòng ảo</p>
            <p className="roboto-font">
              Giúp các doanh nghiệp đăng ký địa chỉ kinh doanh dễ dàng tại các
              vị trí đắc địa.
            </p>
          </div>
        </div> */}
        <div
          dangerouslySetInnerHTML={{
            __html: package_office?.features_of_all_inclusive_offices?.content,
          }}
        />
      </div>
    </div>
  );
}
