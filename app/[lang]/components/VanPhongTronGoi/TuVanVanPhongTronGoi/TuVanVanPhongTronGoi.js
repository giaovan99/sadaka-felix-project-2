import React from "react";
import "../../../../../css/TuVanVanPhongTronGoi.css";
import FormLienHe from "../../FormLienHe/FormLienHe";
// import Link from "next/link";

export default function TuVanVanPhongTronGoi({ dict }) {
  return (
    <div className="TuVanVanPhongTronGoi mt-16 md:mt-[120px] text-white flex items-center">
      <div className="container-1 text-center space-y-2 md:space-y-4">
        <div className="title">
          <h3 className="title--section--3">
            {dict?.package_office?.i_need_advice_on_all_inclusive_office}
          </h3>
        </div>
        <div className="content space-y-3 md:space-y-4 roboto-font">
          <div className="disc ">
            <p>
              {
                dict?.package_office
                  ?.or_contact_us_immediately_0909_440_096_are_you
              }
            </p>
          </div>
          <div className="hotline">
            <a href={`tel:${dict?.setting?.hotline}`}>
              {dict?.setting?.hotline}
            </a>
          </div>
          <div>
            <p>{dict?.general?.or}</p>
          </div>
          <FormLienHe
            text={dict?.general?.button_open_contact_form}
            dict={dict}
          />
        </div>
      </div>
    </div>
  );
}
