import "./globals.css";
import Link from "next/link";
import Script from "next/script";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

export const metadata = {
  title: "Felix Office",
  description:
    "Felix Office cung cấp dịch vụ cho thuê văn phòng, tòa nhà và mặt bằng kinh doanh chuyên nghiệp hàng đầu tại TP. HCM. Giúp bạn tiết kiệm thời gian, tối ưu chi phí với điều kiện thuê tốt nhất!",
};

export async function generateStaticParams() {
  return [{ lang: "en" }, { lang: "vi" }];
}

export default function RootLayout({ children, params }) {
  return (
    <html lang={params.lang}>
      <head>
        <Link rel="preconnect" href="https://fonts.googleapis.com" />
        <Link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <Link
          href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap"
          rel="stylesheet"
        />
        <Link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossOrigin="anonymous"
          referrerPolicy="no-referrer"
        />
        <Link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossOrigin="anonymous"
        />
        {/* <!-- Google tag (gtag.js) --> */}
        <Script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-NCN3NVNZ8P"
        ></Script>
        <Script id="google-analytics">
          {`window.dataLayer = window.dataLayer || []; function gtag()
          {window.dataLayer.push(arguments)} gtag("js", new Date());
          gtag("config", "G-NCN3NVNZ8P");`}
        </Script>
      </head>
      <body>
        <div className="h-[128px] lg:h-[100px] bg-white"></div>
        <Header lang={params.lang} />
        {children}
        <Footer lang={params.lang} />
        <Script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.6/flowbite.min.js"></Script>
      </body>
    </html>
  );
}
