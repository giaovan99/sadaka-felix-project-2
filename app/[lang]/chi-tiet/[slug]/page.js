"use client";
import React, { useEffect, useState } from "react";
import "../../../../css/chi-tiet.css";
import AvailabilityChiTietPage from "../../components/ChiTietPage/AvailabilityChiTietPage/AvailabilityChiTietPage";
import ImageChiTietPage from "../../components/ChiTietPage/ImageChiTietPage/ImageChiTietPage";
import InfoChiTietPage from "../../components/ChiTietPage/InfoChiTietPage/InfoChiTietPage";
import MapChiTietPage from "../../components/ChiTietPage/MapChiTietPage/MapChiTietPage";
import DatLichChiTietPage from "../../components/ChiTietPage/DatLichChiTietPage/DatLichChiTietPage";
import GanDayChiTietPage from "../../components/ChiTietPage/GanDayChiTietPage/GanDayChiTietPage";
import { productService } from "@/service/productService";
import { getDictionary } from "../../distionaries";

export default function Page({ params, searchParams }) {
  const [arr, setArr] = useState([]);
  const [item, setItem] = useState({});
  const [dict, setDict] = useState({});

  const { lang } = params;
  let { quotation } = searchParams;

  let getProductDetail = async () => {
    let res = await productService.getProductDetail(params.slug);
    // console.log(res);
    if (res.data.code == 200) {
      setItem(res.data.data);
    }
  };

  useEffect(() => {
    getProductDetail();
  }, []);

  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);

  let getGanNoiTimKiem = async () => {
    let res = await productService.getProduct({
      limit: 10,
      page: 1,
      type: item.type,
      keywords: "",
      district_id: item.district_id,
      min_price: "",
      max_price: "",
      ward: item?.ward,
    });
    if (res.data.code == 200) {
      let cloneArr = [...res.data.data];
      let index = cloneArr.findIndex((product) => item.id == product.id);
      if (index == -1) {
        setArr(cloneArr);
      } else {
        cloneArr.splice(index, 1);
        setArr(cloneArr);
      }
    }
  };

  useEffect(() => {
    getGanNoiTimKiem();
  }, [item]);

  return (
    <section id="chi-tiet" className=" bg-white">
      {/* Availability */}
      <AvailabilityChiTietPage item={item} quotation={quotation} />
      {/* Image Gallery */}
      {item?.gallery && <ImageChiTietPage gallery={item.gallery} />}
      {/* Info */}
      <InfoChiTietPage item={item} dict={dict} />
      {/* Bản đồ */}
      {item?.link_google_map && <MapChiTietPage item={item} dict={dict} />}
      {/* Đặt lịch */}
      {item?.type == "office_floor" && (
        <DatLichChiTietPage item={item} dict={dict} />
      )}
      {/* Gần đây */}
      <GanDayChiTietPage item={item} arr={arr} dict={dict} />
    </section>
  );
}
