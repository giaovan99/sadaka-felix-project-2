"use client";
import React, { useEffect, useState } from "react";
import ProductListHeader from "../components/ProductList/ProductListHeader/ProductListHeader";
import ProductListSearch from "../components/ProductList/ProductListSearch/ProductListSearch";
import ProductListSubTiltle from "../components/ProductList/ProductListSubTiltle/ProductListSubTiltle";
import ProductListChooseDistrict from "../components/ProductList/ProductListChooseDistrict/ProductListChooseDistrict";
import ProductListRender from "../components/ProductList/ProductListRender/ProductListRender";
import ProductListLienHe from "../components/ProductList/ProductListLienHe/ProductListLienHe";
import ProductListEndPage from "../components/ProductList/ProductListEndPage/ProductListEndPage";
import { productService } from "@/service/productService";
import { getDictionary } from "../distionaries";

export default function Page({ params: { lang } }) {
  // console.log(lang);
  const [dict, setDict] = useState({});
  const { business_premises } = dict;

  // danh sách các quận có tài sảm khi get tất cả sản phẩm
  const [districts, setDistricts] = useState([]);
  // danh sách tất cả các quận tại TPHCM
  const [districtsList, setDistrictsList] = useState([]);
  // danh sách id các quận khi khách hàng chọn quận nhất định
  const [listDistricActive, setListDistricActive] = useState([]);
  // số lượng kết quả
  const [result, setResult] = useState(0);
  // loading page
  const [loading, setLoading] = useState(true);

  let checkActiveDistrict = (id) => {
    // check id quận đã có trong mảng chưa
    let index = listDistricActive.findIndex((item) => item == id);
    let cloneArr = [...listDistricActive];
    // nếu chưa, push id và mảng
    if (index == -1) {
      cloneArr.push(id);
    }
    // nếu rồi, xóa khỏi mảng
    else {
      cloneArr.splice(index, 1);
    }
    setListDistricActive(cloneArr);
  };

  // lấy danh sách sản phẩm
  let getProdustList = async () => {
    const res = await productService.getProduct({
      limit: "",
      page: "",
      type: "business_premises",
      keywords: "",
      district_id: listDistricActive.join(","),
      min_price: "",
      max_price: "",
    });
    setDistricts(res.data.relationship.districts);
    setResult(res.data.total);
    setLoading(false);
  };

  // lay danh sach tat ca cac quan o HCM
  let getDistrictList = async () => {
    const res = await productService.getDistrict();
    if (res.data.code == 200) {
      setDistrictsList(res.data.data);
    }
  };
  useEffect(() => {
    getProdustList();
    getDistrictList();
  }, []);

  useEffect(() => {
    getProdustList();
  }, [listDistricActive]);

  //  Lấy dữ liệu từ dict
  const getDict = async () => {
    let res = await getDictionary(lang);
    if (res) {
      setDict(res);
    }
  };

  useEffect(() => {
    getDict(lang);
  }, [lang]);

  return (
    <section id="mat-bang-kinh-doanh">
      {/* Header */}
      <ProductListHeader text={business_premises?.business_space_header} />
      {/* Search */}
      <ProductListSearch
        type="business_premises"
        districtsList={districtsList}
        dict={dict}
      />
      {/* Sub Title */}
      <ProductListSubTiltle
        title={business_premises?.business_space_for_rent_in_ho_chi_minh_city}
        text={business_premises?.an_ideal_business_premises_is_a_key_factor}
      />
      {/* Choose District */}
      <ProductListChooseDistrict
        checkActiveDistrict={checkActiveDistrict}
        listDistricActive={listDistricActive}
        arr={districtsList}
        setListDistricActive={setListDistricActive}
        result={result}
        setResult={setResult}
        loading={loading}
        setLoading={setLoading}
        dict={dict}
      />
      {/* Render */}
      <ProductListRender
        districts={districts}
        type="business_premises"
        dict={dict}
      />
      {/* Contact  */}
      <ProductListLienHe
        dict={dict}
        question={dict?.general?.havent_found_the_right_space_yet}
      />
      {/* End Page */}
      <ProductListEndPage
        title={dict?.general?.entire_building}
        disc={business_premises?.an_ideal_business_premises_is_a_key_factor}
        text1={business_premises?.area_from_300_to_500}
        text2={business_premises?.area_from_500_to_1000}
        text3={business_premises?.area_over_1000}
        type="building"
        dict={dict}
        questionText={dict?.general?.you_have_not_found_the_right_space}
      />
    </section>
  );
}
